# InventoryProject React Webapp
This is the React based webapp for InventoryProject.
## Building
Start by running `npm install` to install all the dependencies.
After that you can run `npx webpack` to build the development bundle or `npx webpack --config=webpack.config.production.js` to build the production bundle.
### Production
If you choose to build the production bundle you'll have to manually edit `wwwroot/index.html` to load `app/main.js` instead of `app/main.dev.js` and also remove all the node_modules dependencies. Sorry for the inconvenience! This should be done automatically but I'm too lazy to script it.
### Development
If you choose to build the development bundle you'll want to make a symbolic link to node_modules in wwwroot, e.g. `ln -s ../node_modules wwwroot/node_modules`.
## Hosting
Now that you've built the app bundle you can host the app with your web server of choice.
Simply point the server's root directory to `wwwroot` here and remember to make 404 silently redirect to index.html.
And also proxy pass `/api` (and `/swagger` if you want) to where the inventory server is.
Sample nginx.conf:
```
	location / {
		root   /path/to/this/repo/root/wwwroot/;
		index  index.html index.htm;
		try_files $uri /index.html = 404;
	}
	location /api {
			proxy_pass http://127.0.0.1:5000;
	}
	location /swagger {
			proxy_pass http://127.0.0.1:5000;
	}
```
Start the web server, cross your fingers then point your web browser of choice to it. If all is done correctly everything should Just Work™.