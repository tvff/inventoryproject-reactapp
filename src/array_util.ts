// Here are some array functions that are supposedly faster than the built-in find and such.
interface IObjectWithId{
	id:number;
}
/**
 * Finds an object in an array by id.
 * @param array The array to search in.
 * @param id The id to search for.
 * @returns The found object. Returns undefined if not found.
 */
export const findInArrayById = <T extends IObjectWithId>(array:T[],id:number):T => {
	let len = array.length;
	for (let i = 0; i < len;i++){
		if (array[i].id == id)
			return array[i];
	}
	return undefined;
}
interface IQuickFindResult<T>{
	index:number;
	object:T;
}
/**
 * Find an object in an array by id, "quickly". Array needs to be sorted.
 * @param array The array to search in.
 * @param id The id to search for.
 * @returns A result object. Object in result is undefined if not found.
 */
export const quickFindInArrayById = <T extends IObjectWithId>(array:T[],id:number):IQuickFindResult<T> => {
	let len = array.length;
	if (len == 0)
		return {index:0,object:undefined};
	else if (len == 1) {
		let first = array[0];
		if (first.id == id)
			return {index:0,object:first};
		else if (first.id > id)
			return {index:0,object:undefined};
		else
			return {index:1,object:undefined};
	}
	let index = 0
	let max = len-1;
	let diff = max;
	let last = 0;
	let object = array[index];
	while (object.id != id){
		if (object.id > id){
			index -= diff;
			// Prevent going out of bounds.
			if (index < 0)
				return {index:0,object:undefined};
		}
		else{
			index += diff;
			// Here as well.
			if (index > max)
				return {index:len,object:undefined};
		}
		if (diff > 1)
			diff = ~~(diff/2);
		else if (last > 2)
			return{index,object:undefined};
		else
			last++;
		object = array[index];
	}
	// Obviously returning a new object isn't great for performance.. but this makes things more convenient.
	return {index,object};
}