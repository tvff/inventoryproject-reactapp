import * as React from "react";
import { sliceString } from "../util";

export interface IAutoCompletionEntry{
	completion:string;
	description:string;
	value:any;
	id:number;
}

interface IAutoCompletionEntryProps{
	completion:IAutoCompletionEntry;
	click:() => void;
	selected:boolean;
}
const AutoCompletionEntry:React.FunctionComponent<IAutoCompletionEntryProps> = (p) => {
	let desc;
	if (p.completion.description){
		desc = sliceString(p.completion.description,50);
	}
	let comp = sliceString(p.completion.completion,40);
	let className = "clickableListItem completionEntry";
	if (p.selected)
		className += " completionSelected";
	return <div className={className} onClick={() => p.click()}><strong>{comp}</strong>
	{desc &&<span><br/>{desc}</span>}</div>
}

interface IAutoCompleteFieldProps{
	completions:(text:string) => IAutoCompletionEntry[];
	submitText:(text:string) => void;
	submitCompletion:(entry:IAutoCompletionEntry) => void;
	buttonText:string;
	buttonValue:string;
	placeholder:string;
}
interface IAutoCompleteFieldState{
	completions:IAutoCompletionEntry[];
	text:string;
	focused:boolean;
	selected:number;
}
/**
 * A text input field that has hooks for autocompletion.
 */
export class AutoCompleteField extends React.Component<IAutoCompleteFieldProps,IAutoCompleteFieldState>{
	private hideCompletionsTimeout:number;
	constructor(p:any){
		super(p);
		this.state = {completions:[],text:'',focused:false,selected:-1};
	}
	componentWillUnmount(){
		clearTimeout(this.hideCompletionsTimeout);
	}
	resetState(){
		this.setState({completions:[],text:'',selected:-1});
	}
	submit(e: React.FormEvent<HTMLFormElement>){
		e.preventDefault();
		if (this.state.text.length == 0)
			return;
		if (this.state.selected != -1)
			this.props.submitCompletion(this.state.completions[this.state.selected]);
		else
			this.props.submitText(this.state.text);
		this.resetState();
	}
	submitCompletion(e:IAutoCompletionEntry){
		this.props.submitCompletion(e);
		this.resetState();
	}
	textChanged(e: React.FormEvent<HTMLInputElement>){
		let value = e.currentTarget.value;
		if (value == ''){
			this.resetState();
			return;
		}
		this.setState({completions:this.props.completions(value),text:value,selected:-1});
	}
	setFocused(focused:boolean){
		// Cancel the hacky timeout in case user is fast.
		clearTimeout(this.hideCompletionsTimeout);
		this.setState({focused});
	}
	// This is a fugly hack to make clicking completions work as expected.
	setDelayedFocus(focused:boolean){
		this.hideCompletionsTimeout = setTimeout(() => {
			this.setState({focused});
		},200);
	}
	modifySelection(up:boolean){
		let selected = this.state.selected;
		if (up && selected < this.state.completions.length-1)
			this.setState({selected:selected+1});
		else if (!up && selected > -1)
			this.setState({selected:selected-1});
	}
	inputKeyDown(e: React.KeyboardEvent<HTMLInputElement>){
		let key = e.keyCode;
		if (key == 38){
			e.preventDefault();
			this.modifySelection(false);
		}
		else if (key == 40){
			e.preventDefault();
			this.modifySelection(true);
		}
	}
	render(){
		let i = 0;
		let completions = this.state.completions.map(c => {
			return <AutoCompletionEntry selected={i++ == this.state.selected} key={c.id} click={() => this.submitCompletion(c)} completion={c}/>;
		});
		return <div><form onSubmit={e => this.submit(e)}>
			<input onFocus={() => this.setFocused(true)} onKeyDown={e => this.inputKeyDown(e)} onBlur={() => this.setDelayedFocus(false)}
			type="text" value={this.state.text} onChange={e => this.textChanged(e)} placeholder={this.props.placeholder}/>
			<input type="submit" title={this.props.buttonText} value={this.props.buttonValue}/>
			</form>
			{(this.state.focused && this.state.completions.length > 0) && <div className="autoCompleteCompletions">
				{completions}
				</div>}
			</div>
	}

}