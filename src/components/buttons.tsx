import * as React from "react";
import { YesNoDialog } from "./dialog";
import { globals, TG } from "../globals";
interface IConfirmActionButtonProps{
	action:() => void;
	buttonContent:string;
	buttonTitle?:string;
	buttonClass?:string;
}
/**
 * A simple button that asks the user to confirm the action.
 */
export class ConfirmActionButton extends React.Component<IConfirmActionButtonProps,{}>{
	showDialog(){
		globals.popup.showDialog(<YesNoDialog action={e => this.confirmAction(e)}>{this.props.children}</YesNoDialog>);
	}
	confirmAction(act:string){
		if (act == "yes")
			this.props.action();
		globals.popup.hide();
	}
	render(){
		return <input type="button" className={this.props.buttonClass} title={this.props.buttonTitle} value={this.props.buttonContent} onClick={e => this.showDialog()}/>
	}
}

interface IIconButtonProps{
	click:() => void;
	title:string;
	value:string;
	className?:string;
}
/**
 * A fancy button with an icon instead of text.
 */
export const IconButton:React.FunctionComponent<IIconButtonProps> = (p) => {
	let className = "iButton";
	if (p.className)
		className = "iButton "+ p.className;
	return <input type="button" className={className} title={p.title} value={p.value} onClick={() => p.click()}/>
}

interface INevermindButtonProps{
	click:() => void;
}
/**
 * The "I changed my mind..." button.
 */
export const NevermindButton:React.FunctionComponent<INevermindButtonProps> = (p) => {
	return <input type="button" className="nvmButton" value={TG.get("general.nevermind")} onClick={() => p.click()}/>
}
