import * as React from "react";
import { TG } from "../globals";

interface IYesNoDialogProps {
	action:(action:string) => void;
}
/**
 * A "yes/no" dialog! Used for confirming actions.
 */
export const YesNoDialog:React.FunctionComponent<IYesNoDialogProps> = (p) => {
	return <React.Fragment>
		{p.children}
		<div className="dialogButtons">
			<input type="button" value={TG.get("general.yes")} onClick={e => p.action('yes')}/>
			<input className="nvmButton" type="button" value={TG.get("general.no")} onClick={e => p.action('no')}/>
		</div>
	</React.Fragment>
}
/**
 * A basic "take over the screen" dialog that are such common things these days.
 * Probably because they're easy to implement.
 */
export const Dialog:React.FunctionComponent = (p) => {
	return <div className="fullscreenBlocker">
			<div className="dialog">
				{p.children}
			</div>
		</div>
}