import * as React from "react";
import { TG } from "../globals";

export class HomeView extends React.Component<{},{}>{
	render(){
		return <div>
			<h1>InventoryApp!</h1>
			<p>{TG.get("home.intro")}</p>
			<p>{TG.get("home.introContinued")}</p>
		</div>
	}
}