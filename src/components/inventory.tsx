import * as React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { ItemList } from "./item";
import { IState } from "../redux/store";
import { IInventory, IUniqueItem } from "../redux/types";
import { globals, TG } from "../globals";
import { Store } from "../redux/store";
import { EItemAction, IItemAction } from "./itemactions";
import { quickFindInArrayById } from "../array_util";
interface IInventoryProps {
	inventory:IInventory;
}
export const getUniqueItem = (state:IState,itemId:number):IUniqueItem => {
	let uniqueItem = quickFindInArrayById(state.items,itemId).object;
	// Hm, that item does not exist?
	if (uniqueItem == undefined) {
		uniqueItem = { id:itemId,name:"!!! ITEM NOT FOUND !!!",description:"!!! NOT FOUND",ownerId:0,createdAt:0,modifiedAt:0};
		// Ask the service if the item exists. If it doesn't exist it removes this dangling item.
		globals.inventory.getItem(itemId);
	}
	return uniqueItem;
}
export const InventoryHeader:React.FunctionComponent<IInventoryProps> = (p) => {
	return <div className="clickableListItem">{p.inventory.name}</div>
}
export class InventoryContent extends React.Component<IInventoryProps> {
	componentDidMount(){
		this.getInventoryItems();
	}
	getInventoryItems(){
		globals.inventory.getInventoryItems(this.props.inventory);
	}
	componentDidUpdate(pp:IInventoryProps){
		if (this.props.inventory.id != pp.inventory.id){
			this.getInventoryItems();
		}
	}
	render(){
		if (this.props.inventory.items.length == 0)
			return <ItemList detailsInDialog={false} items={[]}/>
		// What am I even doing adding this logic into the render method.
		let curUserId = 0;
		let state = Store.getState();
		if (state.session.user != undefined)
			curUserId = state.session.user.id;
		// It's time to stop.
		let items = this.props.inventory.items.map((i) => {
			const itemId = i.itemId;
			let actions:IItemAction[] = []
			if (this.props.inventory.ownerId == curUserId)
				actions.push({action:EItemAction.RemoveFromInventory, actionData:{inventoryId:this.props.inventory.id,itemId:i.id}});
			else if (curUserId != 0)
				actions.push({action:EItemAction.Trade,actionData:i.id})
			return {itemId,id:i.id,actions,standardActions:curUserId != 0}
		});
		return <ItemList detailsInDialog={false} items={items}/>
	}
}
const mapInventoriesState = (state:IState, p:IInventorySelectorProps) => {
	let inventories = state.inventories;
	if (p.ownerId != undefined){
		inventories = inventories.filter(i => i.ownerId == p.ownerId);
	}
	return {inventories:inventories}
}
interface IInventorySelectorProps {
	selected?:(selected:IInventory) => void;
	inventories?:IInventory[];
	ownerId?:number;
	asLinks:boolean;
}
const InventorySelectorPresenter:React.FunctionComponent<IInventorySelectorProps> = (p) => {
	if (p.inventories.length == 0)
		return <p className="empty">{TG.get("inventory.none")}</p>
	let inventories;
	// This could also be done better. Probably.
	if (p.asLinks)
		inventories = p.inventories.map(i => <Link key={i.id} className="sideBarLink" to={`/inventory/${i.id}`}><div><InventoryHeader inventory={i}/></div></Link>);
	else
		inventories = p.inventories.map(i => <div key={i.id} onClick={() => p.selected(i)}><InventoryHeader inventory={i}/></div>);
	return <div className="inventorySelector">
		{inventories}
	</div>
}
export const InventorySelector = connect(mapInventoriesState)(InventorySelectorPresenter);
