import * as React from "react";
import { match, Link } from "react-router-dom";
import { connect } from "react-redux";
import { IInventory } from "../redux/types";
import { ItemList, IItemListItem } from "./item";
import { InventoryContent } from "./inventory";
import { UserInfoSmall } from "./user";
import { globals, TG } from "../globals";
import { IState, Store } from "../redux/store";
import { ConfirmActionButton, IconButton, NevermindButton } from "./buttons";
import { historyObj } from "./main";
import { AutoCompleteField, IAutoCompletionEntry } from "./autocompletefield";
import { NewEntity } from "./new";
import { findInArrayById } from "../array_util";
// Not a big fan of this, I could just pass the IInventory directly...
// ... but then Redux won't do magic updating..
interface IInventoryViewerProps{
	inventory?:IInventory;
	showModificationButtons:boolean;
	pinned:boolean;
	showPinButton:boolean;
	match?:match<InventoryParams>;
}
type InventoryParams = {id:string}
const mapInventoryViewerState = (state:IState, p:IInventoryViewerProps):IInventoryViewerProps => {
	let showModificationButtons = false;
	let numberedId = Number(p.match.params.id);
	if (numberedId == 0)
		return {inventory:null,showModificationButtons,pinned:false,showPinButton:false}
	const inventory = findInArrayById(state.inventories,numberedId);
	const loggedIn = state.session.user != null;
	if (loggedIn && inventory)
		showModificationButtons = state.session.user.id == inventory.ownerId;
	const pinned = state.userdata.pins.some(p => p == numberedId);
	return {inventory,showModificationButtons,pinned,showPinButton:loggedIn}
}
const deleteInventory = (inv:IInventory) => {
	historyObj.push("/items");
	globals.inventory.deleteInventory(inv);
}
interface IInventoryEditorProps{
	inventory:IInventory;
	done:() => void;
}
const InventoryEditor:React.FunctionComponent<IInventoryEditorProps> = (p) => {
	// Evil refs, again.
	const nameR = React.createRef<HTMLInputElement>();
	const worldVisibleR = React.createRef<HTMLInputElement>();
	const descriptionR = React.createRef<HTMLTextAreaElement>();
	const invName = TG.get("inventory.name");
	const invDescription = TG.get("inventory.description");
	const invWorldVisible = TG.get("inventory.worldVisible");
	const saveChanges = () => {
		globals.inventory.editInventory(p.inventory.id,{name:nameR.current.value,worldVisible:worldVisibleR.current.checked,description:descriptionR.current.value});
		p.done();
	}
	return <div>
		<label>{invName}
			<input required ref={nameR} type="text" defaultValue={p.inventory.name} placeholder={invName}/>
		</label>
		<label className="cbL">
			<input type="checkbox" ref={worldVisibleR} defaultChecked={p.inventory.worldVisible} value="privacy"/>
			{invWorldVisible}
		</label>
		<label>{invDescription}
			<textarea ref={descriptionR} defaultValue={p.inventory.description} placeholder={invDescription}/>
		</label>
		<input type="button" value={TG.get("general.save")} onClick={() => saveChanges()}/>
		<NevermindButton click={() => p.done()}/>
	</div>
}

const inventoryAddNewItem = (name:string,inventoryId:number) => {
	globals.inventory.createItem(name,(item) => {
		globals.inventory.putItemInInventory(item,inventoryId);
	});
}
const getItemCompletions = (text:string):IAutoCompletionEntry[] => {
	// Is calling getState this often a good idea?
	let items = Store.getState().items.filter(i => i.name.toLocaleLowerCase().includes(text.toLocaleLowerCase()));
	return items.map(i => {return {id:i.id,description:i.description,value:i,completion:i.name}});
}
const InventoryViewerPresenter:React.FunctionComponent<IInventoryViewerProps> = (p) => {
	if (p.inventory == null) {
		return <div><h1>{TG.get("inventory.notFound")}</h1>
			<Link to="/items/">{TG.get("general.backToSafety")}</Link></div>
	}
	else {
		const showEditDialog = () => {
			globals.popup.showDialog(<InventoryEditor inventory={p.inventory} done={() => globals.popup.hide()}/>);
		}
	const pinButtonTitle = p.pinned ? TG.get("inventory.unpin") : TG.get("inventory.pin");
	const pinInventory = () => {
		if (p.pinned)
			globals.inventory.unpinInventory(p.inventory.id);
		else
			globals.inventory.pinInventory(p.inventory.id);
	}
	const pinButtonClass = p.pinned ? undefined : "fadedButton"
	return <div><h1>{p.inventory.worldVisible && <span title={TG.get("inventory.worldVisible")}>🌐</span>}{p.inventory.name}
		{p.showPinButton && <IconButton value="📌" className={pinButtonClass} title={pinButtonTitle} click={() => pinInventory()}/>}</h1>
			<h3>{TG.get("user.owner")}: <UserInfoSmall userId={p.inventory.ownerId}/></h3>
			<p className="itemDescription spacer">{p.inventory.description}</p>
			{p.showModificationButtons && <React.Fragment><IconButton click={() => showEditDialog()} value="✏" title={TG.get("inventory.edit")}/>
			<ConfirmActionButton buttonClass="iButton" buttonContent="🗑" buttonTitle={TG.get("inventory.delete")} action={() => deleteInventory(p.inventory)}>
				{TG.ges("general.confirmDelete",p.inventory.name)}
			</ConfirmActionButton>
			<AutoCompleteField buttonText={TG.get("item.addToInventory")} buttonValue="📦➕" placeholder={TG.get("item.name")} submitText={e => inventoryAddNewItem(e,p.inventory.id)}
			submitCompletion={e => globals.inventory.putItemInInventory(e.value,p.inventory.id)} completions={e => getItemCompletions(e)}/>
			</React.Fragment>}
			<InventoryContent inventory={p.inventory}/>
			</div>
	}
}
export const InventoryViewer = connect(mapInventoryViewerState)(InventoryViewerPresenter);
interface AllItemsProps{
	items:IItemListItem[];
	loggedIn:boolean;
}
const mapAllItemsState = (s:IState):AllItemsProps => {
	// Micro-optimization :)
	if (!s.session.user){
		return {loggedIn:false,items:s.items.map((i) => {
			return {id:i.id,itemId:i.id,actions:[]}})}
	}
	const items = s.items.map((i) => {
		return {id:i.id,itemId:i.id,actions:[],standardActions:true}
	});
	return {items,loggedIn:true};
}
const allItemsPresenter:React.FunctionComponent<AllItemsProps> = (p) => {
	return <div><h1>{TG.get("item.all")}</h1>
	{p.loggedIn && <NewEntity placeholder={TG.get("item.newName")} buttonText={TG.get("item.create")} clicked={e => globals.inventory.createItem(e)}/>}
	<ItemList detailsInDialog={false} items={p.items}/></div>
}
export const AllItemsViewer = connect(mapAllItemsState)(allItemsPresenter);
