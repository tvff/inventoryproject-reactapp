import * as React from "react";
import { connect } from "react-redux";
import { IUniqueItem } from "../redux/types";
import { getUniqueItem } from "./inventory";
import { globals, TG } from "../globals";
import { NevermindButton } from "./buttons";
import { Link } from "react-router-dom";
import { stringSortCompare, sliceString } from "../util";
import { ComboBox } from "./menu";
import { ItemDetails } from "./itemdetails";
import { IState } from "../redux/store";
import { IItemAction, ItemButtons, EItemAction } from "./itemactions";
interface IItemEditorProps{
	item:IUniqueItem;
	done:() => void;
}
export const ItemEditor:React.FunctionComponent<IItemEditorProps> = (p) =>{
	const desc = p.item.description ? p.item.description : "";
	const nameRef = React.createRef<HTMLInputElement>();
	const descRef = React.createRef<HTMLTextAreaElement>();
	const itemName = TG.get("item.name");
	const itemDescription = TG.get("item.description");
	const saveChanges = () => {
		globals.inventory.editItem(p.item.id,{name:nameRef.current.value,description:descRef.current.value});
		p.done();
	}
	return <div>
	<label>{itemName}
		<input required type="text" placeholder={itemDescription} ref={nameRef} defaultValue={p.item.name}/>
	</label>
	<label>{itemDescription}
		<textarea placeholder={itemDescription} ref={descRef} defaultValue={desc}/>
	</label>
	<div className="dialogButtons">
		<input type="button" value={TG.get("general.save")} onClick={() => saveChanges()}/>
		<NevermindButton click={() => p.done()}/>
	</div>
	</div>
}
export enum EItemDetailsMode{
	Disabled,
	Page,
	Dialog
}
const getItemDetailsLink = (mode:EItemDetailsMode,item:IUniqueItem) => {
	const showDetailsDialog = () => {
		globals.popup.showDialog(<div>
			<ItemDetails itemId={item.id}/>
			<div className="dialogButtons">
				<input type="button" onClick={() => globals.popup.hide()} value={TG.get("general.gotIt")}/>
			</div>
		</div>);
	}
	switch (mode){
		case EItemDetailsMode.Disabled:
			return item.name;
		case EItemDetailsMode.Dialog:
			return <a className="jsLink" onClick={() => showDetailsDialog()}>{item.name}</a>
		case EItemDetailsMode.Page:
			return <Link to={`/item/${item.id}`}>{item.name}</Link>
	}
}
interface IUniqueItemProps {
	item:IUniqueItem;
	detailsMode:EItemDetailsMode;
	actions:IItemAction[];
}
/**
 * Shows an item nicely.
 */
export const UniqueItemComponent:React.FunctionComponent<IUniqueItemProps> = (p) =>{
	let desc = p.item.description;
	if (desc)
		desc = sliceString(desc,200);
	return <div className="uniqueItem">
		<div className="uniqueItemInner">
			{getItemDetailsLink(p.detailsMode,p.item)}<br/>
			{desc && <p className="itemDescription">{desc}</p>}
		</div>
		{p.actions.length > 0 && <div className="uniqueItemAdditional"><ItemButtons actions={p.actions} item={p.item}/></div>}
	</div>
}

export interface IItemListItem{
	itemId:number;
	item?:IUniqueItem;
	actions: IItemAction[];
	standardActions?:boolean;
	id:number;
}
interface IItemListProps {
	items:IItemListItem[];
	detailsInDialog:boolean;
}
const ItemGrid:React.FunctionComponent<IItemListProps> = (p) => {
	const mapItems = () => {
		let len = p.items.length;
		let elem = [];
		for (let i = 0; i < len; i++){
			let ili = p.items[i];
			elem.push(<UniqueItemComponent detailsMode={p.detailsInDialog ? EItemDetailsMode.Dialog : EItemDetailsMode.Page}
				key={ili.id} item={ili.item} actions={ili.actions}/>)
		}
		return elem;
	}
	return <div className="uniqueItemList">
		{mapItems()}
	</div>
}
const ItemListTableRow:React.FunctionComponent<IUniqueItemProps> = (p) => {
	let desc = p.item.description;
	if (desc)
		desc = sliceString(desc,200);
	return <tr>
		<td className="itemTableName">{getItemDetailsLink(p.detailsMode,p.item)}</td>
		<td className="itemDescription">{desc}</td>
		<td><ItemButtons actions={p.actions} item={p.item}/></td>
	</tr>
}
const ItemTable:React.FunctionComponent<IItemListProps> = (p) => {
	const mapItems = () => {
		let elem = [];
		let len = p.items.length;
		for (let i = 0; i < len; i++){
			let ili = p.items[i];
			if (ili.item == undefined)
				continue;
			elem.push(<ItemListTableRow actions={ili.actions} detailsMode={p.detailsInDialog ? EItemDetailsMode.Dialog : EItemDetailsMode.Page}
				key={ili.id} item={ili.item}/>)
		}
		return elem;
	}
	const tablehead = <thead><tr>
		<th>{TG.get("item.name")}</th>
		<th>{TG.get("item.description")}</th>
		<th></th>
		</tr></thead>
	return <table>{tablehead}
		<tbody>
			{mapItems()}
		</tbody>
	</table>
}
enum EItemListSort{
	Name,
	TimeCreated,
	TimeModified
}
interface ISortSelectorProps{
	changed:(sort:EItemListSort) => void;
	reverseChanged:(reverse:boolean) => void;
	current:EItemListSort;
	reverse:boolean;
}
const SortSelector:React.FunctionComponent<ISortSelectorProps> = (p) => {
	// Evil hardcoded sort methods.
	const sorts = [
		{text:TG.get("general.name"),value:EItemListSort.Name},
		{text:TG.get("general.creationTime"),value:EItemListSort.TimeCreated},
		{text:TG.get("general.modificationTime"),value:EItemListSort.TimeModified}
	]
	// Evil CPU cycle gobbling.
	const curSort = sorts.find(s => s.value == p.current);
	const checkSort = (sort:EItemListSort) => {
		if (sort != null && curSort.value != sort)
			p.changed(sort);
	}
	return <React.Fragment>{TG.get("general.sortBy")}&nbsp;
		<ComboBox items={sorts} value={curSort} onChange={e => checkSort(e.value)}/>
	<label>{TG.get("general.reverseSort")}&nbsp;
		<input type="checkbox" checked={p.reverse} onChange={e => p.reverseChanged(e.currentTarget.checked)}/>
	</label>
	</React.Fragment>
}
interface IItemListState{
	tableView:boolean;
	sort:EItemListSort;
	items:IItemListItem[];
	reverse:boolean;
	filter:string;
	filteredItems:number;
}
export const getStandardItemActions = (item:IUniqueItem,curUserId:number):IItemAction[] => {
	let actions = [];
	if (curUserId != 0)
		actions.push({action:EItemAction.AddToInventory, actionData:curUserId});
	if (curUserId == item.ownerId)
		actions.push({action:EItemAction.Standard});
	return actions;

}
const mapItemListProps = (s:IState,p:IItemListProps):IItemListProps => {
	let items:IItemListItem[] = [];
	// This is more CPU cycle eating, but it enables Redux magic updates.
	// Perhaps I'm too worried about that. Especially considering even the most basic apps are incredibly heavy these days.
	let len = p.items.length;
	for (let i = 0; i < len;i++){
		let ili = p.items[i];
		let item = getUniqueItem(s,ili.itemId);
		let actions = [...ili.actions];
		if (ili.standardActions && s.session.user){
			actions = [...getStandardItemActions(item,s.session.user.id),...actions];
		}
		items.push({
			item,
			itemId: ili.itemId,
			id: ili.id,
			actions
		});
	}
	return {
		detailsInDialog: p.detailsInDialog,
		items
	}
}

class ItemListPresenter extends React.Component<IItemListProps,IItemListState>{
	state = {
		tableView:false,
		sort:EItemListSort.Name,
		items:[] as IItemListItem[],
		reverse:false,
		filter:'',
		filteredItems:0
	}
	componentDidMount(){
		this.reSort(this.state.sort,this.state.reverse,this.state.filter);
	}
	changeView(){
		this.setState({tableView:!this.state.tableView});
	}
	getViewString(){
		if (this.state.tableView)
			return TG.get("general.table");
		else
			return TG.get("general.grid");
	}
	reSort(sort:EItemListSort,reverse:boolean,filter:string){
		let unfiltereditems = Array.from(this.props.items);
		let items = unfiltereditems;
		if (filter.length > 0)
			items = this.filter(items,filter);
		switch (sort){
			case EItemListSort.Name:
				items.sort((a,b) => stringSortCompare(a.item.name,b.item.name));
				break;
			case EItemListSort.TimeCreated:
				items.sort((a,b) => a.item.createdAt - b.item.createdAt);
				break;
			case EItemListSort.TimeModified:
				items.sort((a,b) => a.item.modifiedAt - b.item.modifiedAt);
				break;
			default:
				throw "Invalid sort mode!";
		}
		if (reverse)
			items.reverse();
		this.setState({items,sort,reverse,filter,filteredItems:(unfiltereditems.length-items.length)});
	}
	filter(unfiltereditems:IItemListItem[],filter:string):IItemListItem[]{
		const lcfilter = filter.toLocaleLowerCase();
		let items = unfiltereditems.filter(i => i.item.name.toLocaleLowerCase().includes(lcfilter) ||
			(i.item.description && i.item.description.toLocaleLowerCase().includes(lcfilter)));
		return items;
	}
	update(){
		this.reSort(this.state.sort,this.state.reverse,this.state.filter);
	}
	updateFilterText(e:React.ChangeEvent<HTMLInputElement>){
		this.reSort(this.state.sort,this.state.reverse,e.currentTarget.value);
	}
	componentDidUpdate(pp:IItemListProps,ps:IItemListState){
		if (pp.items != this.props.items || ps.sort != this.state.sort){
			this.update();
		}
	}
	render(){
		let items;
		if (this.props.items.length == 0)
			return <div><p className="empty">{TG.get("item.none")}</p></div>
		if (this.state.tableView)
			items = <ItemTable detailsInDialog={this.props.detailsInDialog} items={this.state.items}/>
		else
			items = <ItemGrid detailsInDialog={this.props.detailsInDialog} items={this.state.items}/>
		return <div><div className="itemListFilterBar"><SortSelector reverse={this.state.reverse} current={this.state.sort}
		reverseChanged={e => this.reSort(this.state.sort,e,this.state.filter)} changed={e => this.reSort(e,this.state.reverse,this.state.filter)}/>
		<input type="text" placeholder={TG.get("general.filter")} value={this.state.filter} onChange={e => this.updateFilterText(e)}/>
		<input type="button" value={this.getViewString()} onClick={() => this.changeView()}/>
		<span className="itemListStats"><strong>{this.state.items.length}</strong> {TG.get("general.items")}
		{this.state.filteredItems != 0 && <React.Fragment> (<strong>{this.state.filteredItems}</strong> {TG.get("general.filtered")})</React.Fragment>}</span></div>
		{items}</div>
	}
}
/**
 * Lists items, either in a simple table or a fancy grid.
 */
export const ItemList = connect(mapItemListProps)(ItemListPresenter);