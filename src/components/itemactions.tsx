import * as React from "react";
import { globals, TG } from "../globals";
import { IUniqueItem } from "../redux/types";
import { IconButton, ConfirmActionButton, NevermindButton } from "./buttons";
import { ItemEditor, UniqueItemComponent, EItemDetailsMode } from "./item";
import { InventorySelector } from "./inventory";
import { historyObj } from "./main";
export enum EItemAction{
	Standard,
	AddToTrade,
	RemoveFromTrade,
	Trade,
	AddToInventory,
	RemoveFromInventory
}
export interface IItemAction<T=any>{
	action:EItemAction;
	actionData?:T;
}
interface IActionButtonProps<T>{
	action:IItemAction<T>;
	item?:IUniqueItem;
}
const ActionButtonEdit:React.FunctionComponent<IActionButtonProps<null>> = (p) => {
	const showItemEditor = () => {
		globals.popup.showDialog(<ItemEditor item={p.item} done={() => globals.popup.hide()}/>);
	}
	return <IconButton value="✏" title={TG.get("item.edit")} click={() => showItemEditor()}/>
}
const ActionButtonDelete:React.FunctionComponent<IActionButtonProps<null>> = (p) => {
	const deleteItem = () => {
		globals.inventory.deleteItem(p.item);
	}
	return <ConfirmActionButton buttonClass="iButton" buttonContent="🗑" buttonTitle={TG.get("item.delete")} action={() => deleteItem()}>
		{TG.ges("general.confirmDeleteThis")} <UniqueItemComponent actions={[]} detailsMode={EItemDetailsMode.Disabled} item={p.item}/>
	</ConfirmActionButton>
}
const ActionButtonAddToInventory:React.FunctionComponent<IActionButtonProps<number>> = (p) => {
	const showInventorySelector = () => {
		// This is a bit sketchy...
		globals.popup.showDialog(<div><p>{TG.ges("item.chooseInventory")}</p> <UniqueItemComponent actions={[]} detailsMode={EItemDetailsMode.Disabled} item={p.item}/>
		<InventorySelector asLinks={false} ownerId={p.action.actionData} selected={e => {
			if (e != undefined)
				globals.inventory.putItemInInventory(p.item,e.id);
			globals.popup.hide();
		}}/>
		<div className="dialogButtons">
			<NevermindButton click={() => globals.popup.hide()}/>
		</div></div>);
	}
	return <IconButton value="📦➕" title={TG.get("item.addToInventory")} click={() => showInventorySelector()}/>
}
interface IActionButtonRemoveData{
	itemId:number;
	inventoryId:number;
}
const ActionButtonRemoveFromInventory:React.FunctionComponent<IActionButtonProps<IActionButtonRemoveData>> = (p) => {
	return <IconButton value="📦➖" title={TG.get("inventory.removeItem")}
	click={() => globals.inventory.removeItemFromInventory(p.action.actionData.itemId,p.action.actionData.inventoryId)}/>
}
export interface IActionButtonClick{
	click:() => void;
}
const ActionButtonRemoveFromTrade:React.FunctionComponent<IActionButtonProps<IActionButtonClick>> = (p) => {
	return <IconButton value="➖" title={TG.get("trade.remove")}
	click={() => p.action.actionData.click()}/>
}
const ActionButtonAddToTrade:React.FunctionComponent<IActionButtonProps<IActionButtonClick>> = (p) => {
	return <IconButton value="➕" title={TG.get("trade.add")}
	click={() => p.action.actionData.click()}/>
}
const ActionButtonTrade:React.FunctionComponent<IActionButtonProps<null>> = (p) => {
	return <IconButton click={() => historyObj.push(`/trade/new/?item=${p.action.actionData}`)} value="🤝" title={TG.get("inventory.tradeItem")}/>
}
interface IItemButtonsProps{
	actions:IItemAction<any>[];
	item:IUniqueItem;
}
/**
 * Action buttons for items. Click them to make magic happen.
 */
export const ItemButtons:React.FunctionComponent<IItemButtonsProps> = (p) => {
	let buttons:React.ReactNode[] = [];
	let index = 0;
	p.actions.forEach(a => {
		switch (a.action){
			case EItemAction.Standard:
				buttons.push(<ActionButtonEdit key={index++} item={p.item} action={a}/>);
				buttons.push(<ActionButtonDelete key={index} item={p.item} action={a}/>);
				break;
			case EItemAction.AddToInventory:
				buttons.push(<ActionButtonAddToInventory key={index} item={p.item} action={a}/>);
				break;
			case EItemAction.Trade:
				buttons.push(<ActionButtonTrade key={index} item={p.item} action={a}/>);
				break;
			case EItemAction.RemoveFromInventory:
				buttons.push(<ActionButtonRemoveFromInventory key={index} item={p.item} action={a}/>);
				break;
			case EItemAction.RemoveFromTrade:
				buttons.push(<ActionButtonRemoveFromTrade key={index} action={a}/>);
				break;
			case EItemAction.AddToTrade:
				buttons.push(<ActionButtonAddToTrade key={index} action={a}/>);
				break;
			default:
				console.log("Unknown action!!",a);
		}
		index++;
	});
	return <React.Fragment>{buttons}</React.Fragment>
}