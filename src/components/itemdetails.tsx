import * as React from "react";
import { match, Link } from "react-router-dom";
import { IUniqueItem } from "../redux/types";
import { IState } from "../redux/store";
import { TG } from "../globals";
import { connect } from "react-redux";
import { UserInfoSmall } from "./user";
import { fromUnixTime, dateToIsoString } from "../util";
import { ItemButtons } from "./itemactions";
import { getStandardItemActions } from "./item";
import { quickFindInArrayById } from "../array_util";
interface IItemStampsProps{
	item:IUniqueItem;
}
const ItemTimeStamps:React.FunctionComponent<IItemStampsProps> = (p) => {
	let createdAt = fromUnixTime(p.item.createdAt);
	return <p>{TG.get("general.createdAt")}: <strong>{dateToIsoString(createdAt)}</strong><br/>
	{p.item.createdAt != p.item.modifiedAt && <React.Fragment>{TG.get("general.modifiedAt")}: <strong>
	{dateToIsoString(fromUnixTime(p.item.modifiedAt))}</strong></React.Fragment>}</p>
}

interface IItemDetailsProps{
	item?:IUniqueItem;
	itemId:number;
	curUserId?:number;
}

const mapItemDetailsProps = (s:IState, p:IItemDetailsProps) => {
	let item = quickFindInArrayById(s.items,p.itemId).object;
	let curUserId = 0;
	if (s.session.user)
		curUserId = s.session.user.id;
	return {item,curUserId}
}
const ItemDetailsPresenter:React.FunctionComponent<IItemDetailsProps> = (p) => {
	if (!p.item) {
		return <div><h1>{TG.get("item.notFound")}</h1>
			<Link to="/items/">{TG.get("general.backToSafety")}</Link></div>
	}
	return <div>
		<h1>{p.item.name}</h1>
		<h3>{TG.get("user.owner")}: <UserInfoSmall userId={p.item.ownerId}/></h3>
		<p className="longItemDescription">{p.item.description}</p>
		<ItemTimeStamps item={p.item}/>
		<ItemButtons actions={getStandardItemActions(p.item,p.curUserId)} item={p.item}/>
	</div>
}
export const ItemDetails = connect(mapItemDetailsProps)(ItemDetailsPresenter);

interface IItemDetailsParams{
	id:string;
}
interface IItemDetailsPageProps{
	match:match<IItemDetailsParams>;
}
export const ItemDetailsView:React.FunctionComponent<IItemDetailsPageProps> = (p) =>{
	return <ItemDetails itemId={Number(p.match.params.id)} />
}