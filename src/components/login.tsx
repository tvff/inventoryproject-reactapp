import * as React from "react";
import { connect } from "react-redux";
import { ISessionAndUser } from "../userservice";
import { IState } from "../redux/store";
import { globals, TG } from "../globals";
import { Link } from "react-router-dom";
import { NevermindButton } from "./buttons";
interface ILoginState{
	username:string;
	password:string;
	statusText:string;
}
interface IRegisterDialogProps{
	done:() => void;
}
// This "thing" makes me sick.
const RegisterDialog:React.FunctionComponent<IRegisterDialogProps> = (p) => {
	let lusername = TG.get("user.name");
	let lfriendlyName = TG.get("user.friendlyName");
	let lpassword = TG.get("user.password");
	let lpasswordConfirm = TG.get("user.passwordConfirm");
	let rusername = React.createRef<HTMLInputElement>();
	let rfriendlyname = React.createRef<HTMLInputElement>();
	let rpassword = React.createRef<HTMLInputElement>();
	let rpasswordConfirm = React.createRef<HTMLInputElement>();
	let rmessage = React.createRef<HTMLSpanElement>();
	let submitf = (e:React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		if (rpasswordConfirm.current.value != rpassword.current.value) {
			rmessage.current.innerText = TG.get("user.passwordConfirmNotMatch");
			return;
		}
		globals.user.register(rusername.current.value,rfriendlyname.current.value,rpassword.current.value).then(e => {
			globals.user.authenticate(e.username,rpassword.current.value).then(e => {
				globals.user.useSessionUser(e);
			});
			p.done();
		}, e => {
			e.then((r: any) => {
				if (r.error)
					rmessage.current.innerText = TG.get(`error.${r.error}`);
			});
		});
		return;
	}
	return <form onSubmit={e => submitf(e)} className="registerForm">
			<label>{lusername}
				<input required ref={rusername} type="text" placeholder={lusername}/>
			</label>
			<label>{lfriendlyName}
				<input required ref={rfriendlyname} type="text" placeholder={lfriendlyName}/>
			</label>
			<label>{lpassword}
				<input required ref={rpassword} type="password" placeholder={lpassword}/>
			</label>
			<label>{lpasswordConfirm}
				<input required ref={rpasswordConfirm} type="password"placeholder={lpasswordConfirm}/>
			</label>
			<span ref={rmessage} className="registerFormMessage errorText"/>
			<div className="dialogButtons">
				<input type="submit" value={TG.get("user.register")}/>
				<NevermindButton click={() => p.done()}/>
			</div>
		</form>
}
export class LoginComponent extends React.Component<any,ILoginState> {
	constructor(p:any){
		super(p);
		this.state={username:'',password:'',statusText:''};
	}
	tryLogin(e:React.FormEvent<HTMLFormElement>){
		e.preventDefault();
		globals.user.authenticate(this.state.username,this.state.password).then(r => this.checkLogin(r), e => this.checkFail(e));
	}
	checkLogin(r:ISessionAndUser){
		if (r.session.token) {
			globals.user.useSessionUser(r);
		}
	}
	//TODO: WTF is this crap? Fix it. Now.
	checkFail(r:Promise<any>){
		r.then(e => {
			if (e.error)
				this.setState({statusText:TG.get(`error.${e.error}`)});
		})
	}
	// Disgusting JS magic could be used here, but TS does not like that.
	nameChanged(e:React.FormEvent<HTMLInputElement>){
		this.setState({username:e.currentTarget.value});
	}
	passwordChanged(e:React.FormEvent<HTMLInputElement>){
		this.setState({password:e.currentTarget.value});
	}
	showRegisterDialog(show:boolean){
		if (show)
			globals.popup.showDialog(<RegisterDialog done={() => this.showRegisterDialog(false)}/>);
		else
			globals.popup.hide();
	}
	render() {
		return <div className="inlineBlock"><form onSubmit={e => this.tryLogin(e)}>
			<span className="errorText">{this.state.statusText}</span>
			<input onChange={e => this.nameChanged(e)} type="text" placeholder={TG.get("user.name")}/>
			<input onChange={e => this.passwordChanged(e)} type="password" placeholder={TG.get("user.password")}/>
			<input type="submit" value={TG.get("user.logIn")}/>
			<input type="button" onClick={e => this.showRegisterDialog(true)} value={TG.get("user.register")}/>
			</form>
			</div>
	}
}
interface ISessionProps {
	loggedin:boolean;
	username:string;
}
const sessionPresenter:React.FunctionComponent<ISessionProps> = (p) => {
	if (p.loggedin)
		return <span><strong><Link to="/user/self">{p.username}</Link></strong>
			<input type="button" onClick={e => globals.user.logOut()} value={TG.get("user.logOut")}/>
			</span>
	else
		return <LoginComponent/>
}
const mapSessionProps = (s:IState):ISessionProps => {
	let name = s.session.user != null ? s.session.user.friendlyName : "guest"
	return {
		loggedin:s.session.user != null,
		username:name
	}
}
export const LoginStatus = connect(mapSessionProps)(sessionPresenter);
