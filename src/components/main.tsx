import * as React from "react";
import { Router, Switch, Route } from "react-router-dom";
import { createBrowserHistory } from "history";
import { Messages } from "./messages";
import { globals, AppState } from "../globals";
import { TopBar } from "./topbar";
import { UserInfo } from "./user";
import { PopupManagerComponent } from "./popupmanager";
import { SideBar, HamburgerButton } from "./sidebar";
import { InventoryViewer, AllItemsViewer } from "./inventoryviewer";
import { CurrentUserInfo } from "./user";
import { ItemDetailsView } from "./itemdetails";
import { HomeView } from "./home";
import { UserListView } from "./users";
import { TradeOffersView } from "./tradeoffers";
import { TradeEditView } from "./tradeedit";
import { TradeView } from "./trade";
export const historyObj = createBrowserHistory();
/**
 * The main component. Where it all begins.
 */
interface IMainState{
	sideBarOpen:boolean;
}
export class MainComponent extends React.Component<any,IMainState> {
	constructor(p:any){
		super(p);
		this.state={sideBarOpen:globals.state == AppState.Normal};
		globals.popup.windowClick = () => {
			this.sidebarAutoClose();
		}
	}
	toggleSidebar(){
		this.setState({sideBarOpen:!this.state.sideBarOpen});
	}
	sidebarAutoClose(){
		if (globals.state == AppState.Small)
			this.setState({sideBarOpen:false});
	}
	render(){
		return <Router history={historyObj}>
				<PopupManagerComponent/>
				<Messages/>
				<TopBar>
					<HamburgerButton click={() => this.toggleSidebar()}/>
				</TopBar>
				<div className="mainBody">
				<SideBar itemClicked={() => this.sidebarAutoClose()} isOpen={this.state.sideBarOpen}/>
				<div className="mainContent">
				<Switch>
					<Route exact path="/" component={HomeView}/>
					<Route path="/user/self" component={CurrentUserInfo}/>
					<Route path="/user/:id" component={UserInfo}/>
					<Route path="/users" component={UserListView}/>
					<Route path="/inventory/:id" component={InventoryViewer}/>
					<Route path="/items" component={AllItemsViewer}/>
					<Route path="/item/:id" component={ItemDetailsView}/>
					<Route path="/trade/new" component={TradeEditView}/>
					<Route path="/trade/:id" component={TradeView}/>
					<Route exact path="/trade" component={TradeOffersView}/>
				</Switch>
				</div>
				</div>
				<footer>
				</footer>
			</Router>
	}
}
