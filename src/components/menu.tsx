import * as React from "react";
import { globals } from "../globals";
interface IMenuItem {
	text:string;
	value:any;
}
interface IMenuComponentProps {
	items:IMenuItem[];
	selected:(action:IMenuItem) => void;
	isOpen:boolean;
}
interface IMenuComponentState{
	selected:number;
}
interface IMenuListItemProps{
	selected:boolean;
	item:IMenuItem;
	onClick:(ev:React.MouseEvent<HTMLLIElement>) => void;
}
const MenuListItem:React.FunctionComponent<IMenuListItemProps> = (p) => {
	let className = "";
	if (p.selected)
		className = "menuItemSelected";
	return <li className={className} onClick={e => p.onClick(e)}>{p.item.text}</li>
}
/**
 * The menu component is a cheeky component that's shown at will whenever and wherever needed.
 */
export class MenuComponent extends React.Component<IMenuComponentProps,IMenuComponentState> {
	menuRef:React.RefObject<HTMLMenuElement> = React.createRef();
	state = {
		selected:-1
	};
	componentDidUpdate(pp:IMenuComponentProps){
		if (this.props.isOpen && !pp.isOpen){
			globals.popup.registerMenu(this);
			this.setState({selected:-1});
			this.menuRef.current.focus();
		}
		else if (!this.props.isOpen && pp.isOpen){
			globals.popup.unregisterMenu(this);
		}
	}
	tryClose(){
		this.props.selected(null);
	}
	selectItem(ev:React.MouseEvent, item:IMenuItem){
		ev.stopPropagation();
		this.props.selected(item);
	}
	// Evil copied code.
	modifySelection(up:boolean){
		let selected = this.state.selected;
		if (up && selected < this.props.items.length-1)
			this.setState({selected:selected+1});
		else if (!up && selected > -1)
			this.setState({selected:selected-1});
	}
	keyDown(e: React.KeyboardEvent<HTMLElement>) {
		let key = e.keyCode;
		switch (key){
			case 38:
				e.preventDefault();
				this.modifySelection(false);
				break;
			case 40:
				e.preventDefault();
				this.modifySelection(true);
				break;
			// Space and and enter for selecting, for now.
			case 32:
			case 13:
				e.preventDefault();
				if (this.state.selected != -1)
					this.props.selected(this.props.items[this.state.selected]);
				else
					this.props.selected(null);
				break;
		}
	}
	render(){
		if (!this.props.isOpen)
			return null;
		return <menu onKeyDown={e => this.keyDown(e)} tabIndex={0} className="popupMenu" ref={this.menuRef}>
				<ul>
					{this.props.items.map((i,id) => <MenuListItem onClick={e => this.selectItem(e,i)} item={i} selected={id == this.state.selected} key={id}/>)}
				</ul>
			</menu>
	}

}


interface IComboBoxState{
	isOpen:boolean;
}
interface IComboBoxProps{
	onChange:(newVal:IMenuItem) => void;
	value:IMenuItem;
	items:IMenuItem[];
}
/**
 * A combobox, similiar to the HTML <select> element but with custom styling and non-working accessibility features.
 * Hooray for progress.
 */
export class ComboBox extends React.Component<IComboBoxProps,IComboBoxState>{
	state={
		isOpen:false
	};
	handleAction(action:IMenuItem){
		this.setState({isOpen:false});
		if (action != null && action != this.props.value)
			this.props.onChange(action)
	}
	open(e:React.MouseEvent){
		// Hacketyhack..?
		// Using window.onclick to automagically close menus, so have to stop propagation here. Else it instantly closes.
		e.stopPropagation();
		this.setState({isOpen:!this.state.isOpen});
	}
	render(){
		return <div className="comboBoxOuter">
			<button type="button" className="comboBox" onClick={e => this.open(e)}>
				{this.props.value.text}
			</button>
			<MenuComponent selected={e => this.handleAction(e)}
			items={this.props.items} isOpen={this.state.isOpen}/>
		</div>
	}
}