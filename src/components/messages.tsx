import * as React from "react";
import { TG, globals } from "../globals";
interface IMessage{
	msg:string;
	id:number;
}
interface IMessagesState{
	messages:IMessage[];
}
interface IMessageProps{
	message:IMessage;
	acknowledged:() => void;
}
const Message:React.FunctionComponent<IMessageProps> = (p) => {
	return <div className="message">
		<div className="messageContent">
			<p>{p.message.msg}</p>
		</div>
			<input type="button" value={TG.get("general.gotIt")} onClick={() => p.acknowledged()}/>
	</div>
}
/** 
 * This component shows messages. Mostly of the error variety.
 */
export class Messages extends React.Component<any,IMessagesState>{
	constructor(p:any){
		super(p);
		this.state= { messages:[]}
	}
	componentDidMount(){
		globals.http.cbErrorString = (e) => {
			this.addMessage(e);
		}
		globals.http.cbErrorJson = (e) => {
			let stringed = `${e.error}: ${e.message}`
			this.addMessage(stringed);
		}
	}
	componentWillUnmount(){
		globals.http.cbErrorJson = (e) => {};
		globals.http.cbErrorString = (e) => {};
	}
	addMessage(e:any){
		let msgs = Array.from(this.state.messages);
		let msgId = msgs.reduce((a,b) => a.id>b.id ? a : b, {id:0}).id;
		msgs.push({msg:e,id:msgId+1})
		this.setState({messages:msgs});
	}
	removeMessage(message:number){
		let msg = this.state.messages.findIndex(m => m.id == message);
		let msgs = Array.from(this.state.messages);
		msgs.splice(msg,1);
		this.setState({messages:msgs});
	}
	render(){
		return <div className="messagesContainer">
			{this.state.messages.map(m => <Message key={m.id} message={m} acknowledged={() => this.removeMessage(m.id)}/>)}
		</div>
	}
}
