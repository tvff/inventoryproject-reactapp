import * as React from "react";
interface INewEntityProps{
	placeholder:string;
	buttonText:string;
	clicked:(inputText:string) => void;
}
interface INewEntityState{
	text:string;
}
/**
 * Generic component for creating new items and inventories. Probably very placeholder.
 */
export class NewEntity extends React.Component<INewEntityProps,INewEntityState>{
	constructor(p:any){
		super(p);
		this.state = {text:''};
	}
	textChanged(e:React.FormEvent<HTMLInputElement>){
		this.setState({text:e.currentTarget.value});
	}
	submit(e: React.FormEvent<HTMLFormElement>): void {
		e.preventDefault();
		if (this.state.text == '')
			return;
		this.props.clicked(this.state.text);
		this.setState({text:''});
	}
	render(){
		return <form style={{display:'flex',maxWidth:'20em'}} onSubmit={e => this.submit(e)}>
			<input style={{flexGrow:1}} type="text" value={this.state.text} onChange={e => this.textChanged(e)} placeholder={this.props.placeholder}/>
			<input type="submit" title={this.props.buttonText} value="➕"/>
			</form>
	}

}
