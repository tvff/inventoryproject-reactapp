import * as React from "react";
import { globals } from "../globals";
import { Dialog } from "./dialog";
import { historyObj } from "./main";
import { MenuComponent } from "./menu";
interface IPopupManagerCState{
	content:React.ReactElement[];
}
/**
 * Manages popups that might take over the screen. Dialogs and... dialogs.
 * There can only be one!
 */
export class PopupManagerComponent extends React.Component<any,IPopupManagerCState>{
	state={
		content:[] as React.ReactElement[]
	}
	componentDidMount(){
		globals.popup.setComponent(this);
	}
	showDialog(dialog:React.ReactElement){
		let content = Array.from(this.state.content);
		content.push(dialog);
		this.setState({content});
	}
	hide(){
		let content = Array.from(this.state.content);
		content.pop();
		this.setState({content});
	}
	hideAll(){
		this.setState({content:[]});
	}
	render(){
		const len = this.state.content.length;
		if (len == 0)
			return null;
		return <Dialog>{this.state.content[len-1]}</Dialog>
	}
}
/**
 * Is used for interacting with the PopupManagerComponent without all the bloat from React.Component.
 * (Some kind of injection might be a better way of doing this...)
 */
export class PopupManager{
	private component:PopupManagerComponent;
	private menus:MenuComponent[] = [];
	windowClick:() => void;
	constructor(){
		window.onclick = e => {
			this.menus.forEach(m => m.tryClose());
			this.windowClick();
		}
	}
	setComponent(component:PopupManagerComponent){
		if (this.component != undefined) {
			throw "I said there can only be one PopupManagerComponent!"
		}
		this.component = component;
		// Hacketyhack, ensure dialogs go away if any navigation happens.
		historyObj.listen((l,a) => {
			this.hideAll();
		});
	}
	showDialog(content:React.ReactElement){
		this.component.showDialog(content);
	}
	// This menu stuff is really sketchy.
	registerMenu(menu:MenuComponent){
		this.menus.push(menu);
	}
	unregisterMenu(menu:MenuComponent){
		let oldmenu = this.menus.findIndex(m => m == menu);
		this.menus.splice(oldmenu,1);
	}
	hide(){
		this.component.hide();
	}
	hideAll(){
		this.component.hideAll();
	}
}
