import * as React from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { globals, TG } from "../globals";
import { IState } from "../redux/store";
import { IInventory } from "../redux/types";
import { NewEntity } from "./new";
import { stringSortCompare } from "../util";
import { historyObj } from "./main";
interface IInventoryListProps {
	inventories:IInventory[];
	click:() => void;
}
export const InventoriesList:React.FunctionComponent<IInventoryListProps> = (p) => {
	return <div>
		{p.inventories.map(i => <NavLink onClick={() => p.click()} className="sideBarLink" key={i.id} to={`/inventory/${i.id}`}>
			<div className="clickableListItem">{i.name}</div></NavLink>)}
	</div>
}
interface ISideBarLinkProps{
	to:string;
	text:string;
	exact?:boolean;
	onClick:() => void;
}
const SideBarLink:React.FunctionComponent<ISideBarLinkProps> = (p) => {
	return <NavLink exact={p.exact} onClick={() => p.onClick()} className="sideBarLink" to={p.to}>
		<div className="clickableListItem"><strong>{p.text}</strong></div>
	</NavLink>
}
const sortInventories = (invs:IInventory[]) => {
	let temp = Array.from(invs);
	temp.sort((a,b) => stringSortCompare(a.name,b.name));
	return temp;
}
interface ISideBarInventoryGroupProps{
	headerText:string;
	inventories:IInventory[];
	itemClicked:() => void;
}
interface ISideBarInventoryGroupState{
	isOpen:boolean;
}
class SideBarInventoryGroup extends React.Component<ISideBarInventoryGroupProps,ISideBarInventoryGroupState>{
	state={
		isOpen:true
	}
	toggle(){
		this.setState({isOpen:!this.state.isOpen});
	}
	headerPrefix(){
		const openPrefix = "➘";
		const closedPrefix = "➙";
		return this.state.isOpen ? openPrefix : closedPrefix;
	}
	render(){
		if (this.props.inventories.length == 0)
			return null;
		return <React.Fragment><div className="clickableListItem sideBarHeader" onClick={() => this.toggle()}>
			<span className="sideBarHeaderSpan">{this.headerPrefix()}</span>{this.props.headerText}</div>
			{this.state.isOpen && <InventoriesList click={() => this.props.itemClicked()}
			inventories={this.props.inventories}/>}
		</React.Fragment>
	}

}
interface ISideBarProps{
	isOpen?:boolean;
	ownInventories?:IInventory[];
	otherInventories?:IInventory[];
	pinnedInventories?:IInventory[];
	loggedIn?:boolean;
	itemClicked?:() => void;
}
interface ISideBarState{
	showOwnInventories:boolean;
	showOtherInventories:boolean;
	showPins:boolean;
}
const MapSideBarProps = (s:IState,p:ISideBarProps):ISideBarProps => {
	if (!s.session.user)
		return {pinnedInventories:[],ownInventories:[],otherInventories:sortInventories(s.inventories),loggedIn:false};
	// This can probably be done better.
	let own = s.inventories.filter(i => i.ownerId == s.session.user.id);
	let other = s.inventories.filter(i => i.ownerId != s.session.user.id);
	let pinned = s.inventories.filter(i => s.userdata.pins.some(p => p == i.id));
	return {pinnedInventories:sortInventories(pinned),ownInventories:sortInventories(own),
		otherInventories:sortInventories(other),loggedIn:true};
}
class SideBarPresenter extends React.Component<ISideBarProps,ISideBarState> {
	constructor(p:any){
		super(p);
		this.state={showOtherInventories:true,showOwnInventories:true,showPins:true};
	}
	toggleOwn(){
		this.setState({showOwnInventories:!this.state.showOwnInventories});
	}
	toggleOthers(){
		this.setState({showOtherInventories:!this.state.showOtherInventories});
	}
	togglePins(){
		this.setState({showPins:!this.state.showPins});
	}
	headerPrefix(open:boolean){
		const openPrefix = "➘";
		const closedPrefix = "➙";
		return open ? openPrefix : closedPrefix;
	}
	goToInv(inv:IInventory){
		historyObj.push(`/inventory/${inv.id}`);
		this.props.itemClicked();
	}
	sbClick(e: React.MouseEvent<HTMLDivElement, MouseEvent>): void {
		e.stopPropagation();
	}
	render(){
		let className="sideBar";
		if (!this.props.isOpen)
			className="sideBarClosed";
		return <div onClick={e => this.sbClick(e)} className={className}>
			<SideBarLink exact onClick={() => this.props.itemClicked()} to="/" text={TG.get("general.home")}/>
			<SideBarLink onClick={() => this.props.itemClicked()} to="/items" text={TG.get("item.all")}/>
			<SideBarLink onClick={() => this.props.itemClicked()} to="/users" text={TG.get("user.all")}/>
			{this.props.loggedIn && <SideBarLink exact onClick={() => this.props.itemClicked()} to="/trade" text={TG.get("trade.offers")}/>}
			{this.props.loggedIn && <NewEntity placeholder={TG.get('inventory.newName')} buttonText={TG.get("inventory.create")}
			clicked={e => globals.inventory.createInventory({name:e,worldVisible:false,description:null},i => this.goToInv(i))}/>}
			<SideBarInventoryGroup headerText={TG.get("inventory.pins")} itemClicked={() => this.props.itemClicked()} inventories={this.props.pinnedInventories}/>
			<SideBarInventoryGroup headerText={TG.get("inventory.yours")} itemClicked={() => this.props.itemClicked()} inventories={this.props.ownInventories}/>
			<SideBarInventoryGroup headerText={TG.get("inventory.others")} itemClicked={() => this.props.itemClicked()} inventories={this.props.otherInventories}/>
		</div>
	}

}
export const SideBar = connect(MapSideBarProps)(SideBarPresenter);
interface IHamburgerButtonProps {
	click:() => void;
}
export const HamburgerButton:React.FunctionComponent<IHamburgerButtonProps> = (p) => {
	const click = (e: React.MouseEvent) => {
		e.stopPropagation();
		p.click();
	}
	return <input className="iButton" type="button" value="🍔" title={TG.get("general.showHideSideBar")} onClick={(e) => click(e)}/>
}
