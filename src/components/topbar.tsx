import * as React from "react";
import { LoginStatus } from "./login";

export class TopBar extends React.Component<any,any>{
	render(){
		return <header>
			{this.props.children}
			<LoginStatus/>
		</header>
	}
}
