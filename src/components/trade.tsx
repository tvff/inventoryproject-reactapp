import * as React from "react";
import { match, Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { IState, Store } from "../redux/store";
import { ITradeOffer, IOwnedItem } from "../redux/types";
import { globals, TG } from "../globals";
import { IItemListItem, ItemList } from "./item";
import { NevermindButton } from "./buttons";
import { UserInfoSmall } from "./user";
import { InventorySelector } from "./inventory";
import { findInArrayById } from "../array_util";
interface ITradeViewMatch{
	id:string;
}
interface ITradeViewProps{
	match?:match<ITradeViewMatch>;
	offer?:ITradeOffer;
	offerId?:number;
	sent:boolean;
}
interface ITradeViewState{
	redirectUri:string;
}
const mapTradeViewProps = (s:IState,p:ITradeViewProps):ITradeViewProps => {
	let offerId = Number(p.match.params.id);
	if (!offerId || s.session.user == null)
		return {offer:null,offerId:0,sent:false}
	let offer = findInArrayById(s.userdata.tradeoffers,offerId);
	let sender = 0;
	if (offer)
		sender = offer.sender;
	return {offer,offerId,sent:s.session.user.id == sender}
}
class TradeViewPresenter extends React.Component<ITradeViewProps,ITradeViewState>{
	constructor(p:any){
		super(p);
		this.state = {redirectUri:null}
	}
	componentDidMount(){
		if (this.props.offerId != 0){
			this.checkUpdate();
		}
	}
	checkUpdate(){
		if (this.props.offer && this.props.offer.desiredItems && this.props.offer.offeredItems)
			return; // No need, we already have everything.
		globals.inventory.getTradeOffer(this.props.offerId);
	}
	componentDidUpdate(pp:ITradeViewProps){
		if (this.props.offerId != pp.offerId){
			this.checkUpdate(); // Is this even needed?
		}
	}
	createItemListItem(item:IOwnedItem):IItemListItem{
		return {
			itemId:item.itemId,
			id:item.id,
			actions:[]
		}
	}
	deleteOffer(){
		globals.inventory.deleteTradeOffer(this.props.offerId, () => {
			this.setState({redirectUri:'/trade'});	
		});
	}
	declineOffer(){
		globals.inventory.rejectTradeOffer(this.props.offerId,() => {
			this.setState({redirectUri:'/trade'});
		});
	}
	acceptOffer(){
		// Ask for inventory...
		// .. but only if necessary.
		if (this.props.offer.offeredItems.length == 0){
			globals.inventory.acceptTradeOffer(this.props.offerId,0,() => {
				this.setState({redirectUri:'/trade'});
			});
			return;
		}
		let state = Store.getState();
		// This is mostly copy pasted, should make this into a proper component..
		globals.popup.showDialog(<div><p>{TG.ges("trade.chooseInventory")}</p>
			<InventorySelector asLinks={false} ownerId={state.session.user.id} selected={e => {
			if (e != undefined)
				globals.inventory.acceptTradeOffer(this.props.offerId,e.id,() => {
					this.setState({redirectUri:`/inventory/${e.id}`});
				});
			globals.popup.hide();
		}}/>
		<div className="dialogButtons">
			<NevermindButton click={() => globals.popup.hide()}/>
		</div></div>);
	}
	render(){
		if (this.state.redirectUri)
			return <Redirect to={this.state.redirectUri}/>
		// I'm doing this in the render method again...
		if (!this.props.offer)
			return <div>
				<h1>{TG.get("trade.notFoundOrLoading")}</h1>
				<p><Link to="/trade">{TG.get("general.backToSafety")}</Link></p>
			</div>
		let desiredItems;
		let offeredItems;
		if (this.props.offer.desiredItems){
			desiredItems = this.props.offer.desiredItems.map(i => this.createItemListItem(i));
		}
		if (this.props.offer.offeredItems){
			offeredItems = this.props.offer.offeredItems.map(i => this.createItemListItem(i));
		}
		return <div>
			<h1>{TG.get("trade.from")}&nbsp;<UserInfoSmall userId={this.props.offer.sender}/>&nbsp;{TG.get("trade.to")}&nbsp;
			<UserInfoSmall userId={this.props.offer.receiver}/></h1>
			<p className="itemDescription spacer">{this.props.offer.message}</p>
			{desiredItems && <React.Fragment><h2>{TG.get("trade.desired")}</h2>
			<ItemList detailsInDialog={true} items={desiredItems}/></React.Fragment>}
			{offeredItems && <React.Fragment><h2>{TG.get("trade.offered")}</h2>
			<ItemList detailsInDialog={true} items={offeredItems}/></React.Fragment>}
			{this.props.sent && <input type="button" onClick={() => this.deleteOffer()} value={TG.get("trade.delete")}/>}
			{!this.props.sent && <React.Fragment><input type="button" onClick={() => this.acceptOffer()} value={TG.get("trade.accept")}/>
			<input type="button" onClick={() => this.declineOffer()} value={TG.get("trade.decline")}/>
			</React.Fragment>}
		</div>
	}
}
export const TradeView = connect(mapTradeViewProps)(TradeViewPresenter);
