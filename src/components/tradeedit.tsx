import * as React from "react";
import { match, Redirect, Link } from "react-router-dom";
import { connect } from "react-redux";
import { Location } from "history";
import { parseQueryString } from "../util";
import { IOwnedItem, IInventory } from "../redux/types";
import { globals, TG } from "../globals";
import { ItemList, IItemListItem } from "./item";
import { Store, IState } from "../redux/store";
import { InventorySelector } from "./inventory";
import { EItemAction } from "./itemactions";
import { UserSelector } from "./userselector";
import { NevermindButton } from "./buttons";
import { findInArrayById } from "../array_util";

interface ITradeViewProps{
	match:match;
	location:Location;
	enabled:boolean;
}
interface ITradeViewState{
	offeredItems:IOwnedItem[];
	desiredItems:IOwnedItem[];
	currentUserId:number;
	currentInventoryId:number;
	ownInventories:boolean;
	destinationInventory:IInventory;
	message:string;
	redirectUri:string;
}
interface ITradeViewQueryParams{
	item:string; // String here because that's what the query parser returns.
}
const mapTradeViewProps = (s:IState,p:ITradeViewProps):ITradeViewProps => {
	return {match:p.match,location:p.location,enabled:s.session.user != null};
}
class TradeEditViewPresenter extends React.Component<ITradeViewProps,ITradeViewState>{
	constructor(p:any){
		super(p);
		this.state = {
			offeredItems:[],
			desiredItems:[],
			currentUserId:0,
			currentInventoryId:0,
			ownInventories:false,
			destinationInventory:null,
			message:'',
			redirectUri:null
		};
	}
	componentDidMount(){
		if (this.props.enabled)
			this.update();
	}
	update(){
		this.setState({desiredItems:[],offeredItems:[]});
		if (this.props.location.search.length > 0){
			let store = Store.getState();
			const query:ITradeViewQueryParams = parseQueryString(this.props.location.search)
			globals.inventory.getOwnedItem(Number(query.item), (i) => {
				this.addItemToTrade(i);
				// Find the inventory as well..
				let inv = store.inventories.find(inv => inv.id == i.inventoryId);
				if (inv.ownerId != store.session.user.id)
					this.setState({currentUserId:inv.ownerId});
				else
					this.setState({ownInventories:true})
				this.setInventory(inv);
			});
		}
	}
	componentDidUpdate(pp:ITradeViewProps){
		if (!pp.enabled)
			this.update();
	}
	toggleInventories(){
		this.setState({ownInventories:!this.state.ownInventories});
	}
	createItemListItem(item:IOwnedItem):IItemListItem{
		return {
			actions:[{action: EItemAction.RemoveFromTrade, actionData:{click:() => this.removeItemFromTrade(item)}}],
			itemId:item.itemId,
			id:item.id,
		};
	}
	addItemToTrade(item:IOwnedItem){
		let state = Store.getState();
		let inv = findInArrayById(state.inventories,item.inventoryId);
		if (inv.ownerId == state.session.user.id){
			let offeredItems = Array.from(this.state.offeredItems);
			offeredItems.push(item);
			this.setState({offeredItems});
		}
		else {
			let desiredItems = Array.from(this.state.desiredItems);
			desiredItems.push(item);
			this.setState({desiredItems});
		}
	}
	removeItemFromTrade(item:IOwnedItem){
		let inDesired = this.state.desiredItems.findIndex(i => i.id == item.id);
		if (inDesired != -1){
			let desiredItems = Array.from(this.state.desiredItems);
			desiredItems.splice(inDesired,1);
			this.setState({desiredItems});
			return;
		}
		let inOffered = this.state.offeredItems.findIndex(i => i.id == item.id);
		if (inOffered != -1){
			let offeredItems = Array.from(this.state.offeredItems);
			offeredItems.splice(inOffered,1);
			this.setState({offeredItems});
		}
	}
	setInventory(inv: IInventory) {
		globals.inventory.getInventoryItems(inv,() => {
			this.setState({currentInventoryId:inv.id});
		});
	}
	createItemSelectorItems(state: IState):IItemListItem[]{
		let inv = findInArrayById(state.inventories,this.state.currentInventoryId);
		if (!inv)
			return [];
		let items:IItemListItem[] = [];
		inv.items.forEach(i => {
			// This could and should be done better.
			if (this.state.desiredItems.find(b => b.id == i.id) || this.state.offeredItems.find(b => b.id == i.id))
				return;
			items.push({itemId:i.itemId,id:i.id,
				actions:[{action: EItemAction.AddToTrade, actionData:{click:() => this.addItemToTrade(i)}}],
			});
		});
		return items;
	}
	postTradeOffer(){
		globals.inventory.postTradeOffer({
			desiredItems: this.state.desiredItems.map(i => i.id),
			offeredItems: this.state.offeredItems.map(i => i.id),
			type:0,
			message:this.state.message,
			receiver:this.state.currentUserId,
			destination:this.state.destinationInventory.id
		},e => {
			this.setState({redirectUri:`/trade/${e}`});
		});
	}
	chooseDestination(){
		let state = Store.getState();
		globals.popup.showDialog(<div><p>{TG.ges("trade.chooseDesiredInventory")}</p>
			<InventorySelector asLinks={false} ownerId={state.session.user.id} selected={e => {
			if (e != undefined){
				this.setState({destinationInventory:e});
			}
			globals.popup.hide();
		}}/>
		<div className="dialogButtons">
			<NevermindButton click={() => globals.popup.hide()}/>
		</div></div>);
	}
	updateMessage(e: React.ChangeEvent<HTMLTextAreaElement>){
		this.setState({message:e.currentTarget.value});
	}
	render(){
		if (this.state.redirectUri)
			return <Redirect to={this.state.redirectUri}/>
		// Fugly hack here to get this going quickly.
		if (!this.props.enabled)
			return <div>
				<h1>{TG.get("general.needLogin")}</h1>
				<Link to="/">{TG.get("general.backToSafety")}</Link>
			</div>
		let state = Store.getState();
		let desiredItems = this.state.desiredItems.map(i => this.createItemListItem(i));
		let offeredItems = this.state.offeredItems.map(i => this.createItemListItem(i));
		let disableSubmit = (desiredItems.length == 0 && offeredItems.length == 0) ||
			(desiredItems.length > 0 && this.state.destinationInventory == null);
		// More fugly hackage.
		let items = this.createItemSelectorItems(state);
		const offerMessage = TG.get("trade.message");
		const offerDestination = TG.get("trade.destination");
		const offerDestinationText = this.state.destinationInventory != null ? this.state.destinationInventory.name : TG.get("trade.noDestination");
		return <div className="tradeUIContainer">
			<div>
				<div>
				<button type="button" onClick={() => this.toggleInventories()}>
					{this.state.ownInventories ? TG.get("inventory.yours") : TG.get("inventory.their")}
				</button>
				<UserSelector current={this.state.currentUserId}/>
				</div>
				<div className="inlineBlock">
					<InventorySelector ownerId={this.state.ownInventories?state.session.user.id:this.state.currentUserId}
					asLinks={false} selected={inv => this.setInventory(inv)}/>
				</div>
				<ItemList detailsInDialog={true} items={items}/>
			</div>
			<div>
				<h2>{TG.get("trade.desired")}</h2>
				<ItemList detailsInDialog={true} items={desiredItems}/>
				<h2>{TG.get("trade.offered")}</h2>
				<ItemList detailsInDialog={true} items={offeredItems}/>
				<label>{offerMessage}
					<textarea value={this.state.message} placeholder={offerMessage} onChange={(e) => this.updateMessage(e)}/>
				</label>
				<span>{offerDestination}&nbsp;</span>
				<input type="button" value={offerDestinationText} onClick={() => this.chooseDestination()}/>
				<br/>
				<input type="button" value={TG.get("trade.send")} disabled={disableSubmit} onClick={() => this.postTradeOffer()}/>
			</div>
		</div>
	}
}
export const TradeEditView = connect(mapTradeViewProps)(TradeEditViewPresenter);