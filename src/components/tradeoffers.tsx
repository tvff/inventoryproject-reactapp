import * as React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { TG, globals } from "../globals";
import { ITradeOffer } from "../redux/types";
import { UserInfoSmall } from "./user";
import { IState } from "../redux/store";

const TradeOffer:React.FunctionComponent<{offer:ITradeOffer}> = (p) => {
	return <ul>
		<li><UserInfoSmall userId={p.offer.sender}/> 🡲 <UserInfoSmall userId={p.offer.receiver}/></li>
		<li>{p.offer.message}</li>
		<li><Link to={`/trade/${p.offer.id}`}>{TG.get("trade.view")}</Link></li>
	</ul>
}
interface ITradeOffersListProps{
	offers:ITradeOffer[];
}
const mapTradeOffersListProps = (s:IState):ITradeOffersListProps => {
	return {offers:s.userdata.tradeoffers};
}
const TradeOffersListPresenter:React.FunctionComponent<ITradeOffersListProps> = (p) => {
	return <div>
		{p.offers.map(o => <TradeOffer offer={o} key={o.id}/>)}
	</div>
}
const TradeOffersList = connect(mapTradeOffersListProps)(TradeOffersListPresenter);
export class TradeOffersView extends React.Component{
	componentDidMount(){
		globals.inventory.getTradeOffers();
	}
	render(){
		return <div>
		<h1>{TG.get("trade.offers")}</h1>
		<p>{TG.get("trade.description")}</p>
		<TradeOffersList/>
	</div>
	}
}