import * as React from "react";
import { connect } from "react-redux";
import { match, NavLink } from "react-router-dom";
import { globals } from "../globals";
import { IState } from "../redux/store";
import { TG } from "../globals";
import { IUser, ISession, IInventory } from "../redux/types";
import { NevermindButton } from "./buttons";
import { InventorySelector } from "./inventory";
import { historyObj } from "./main";
import { dateToIsoString, fromUnixTime } from "../util";
import { ItemList, IItemListItem } from "./item";
interface IUserDialogProps{
	done:() => void;
}
const DeleteUserConfirm:React.FunctionComponent<IUserDialogProps> = (p) => {
	let pwRef = React.createRef<HTMLInputElement>();
	let messageRef = React.createRef<HTMLSpanElement>();
	let deleteUser = () => {
		globals.user.deleteUser(pwRef.current.value).then(() => {
			// Uuuuggghhh... all this so I can show a message nicely. Need to fix now.
			globals.user.userDeleted();
			p.done();
		},e => {
			e.then((r:any) => {
				messageRef.current.innerText = TG.get(`error.${r.error}`);
			});
		});
	}
	const passwordText = TG.get("user.password");
	return <div>
		<label>{passwordText}
			<input type="password" ref={pwRef} placeholder={passwordText}/>
		</label>
		<span ref={messageRef} className="errorText registerFormMessage"/>
		<div className="dialogButtons">
			<input type="button" value={TG.get("general.doIt")} onClick={(e) => deleteUser()}/>
			<NevermindButton click={() => p.done()}/>
		</div>
		</div>
}
const ChangePassword:React.FunctionComponent<IUserDialogProps> = (p) => {
	let oldPwRef = React.createRef<HTMLInputElement>();
	let newPwRef = React.createRef<HTMLInputElement>();
	let newPwConfirmRef = React.createRef<HTMLInputElement>();
	let messageRef = React.createRef<HTMLSpanElement>();
	let oldPw = TG.get("user.oldPassword");
	let newPw = TG.get("user.password");
	let newPwConfirm = TG.get("user.passwordConfirm");
	let changePassword = (e:React.FormEvent<HTMLFormElement>) => {
		e.preventDefault();
		if (newPwRef.current.value != newPwConfirmRef.current.value){
			messageRef.current.innerText = TG.get("user.passwordConfirmNotMatch");
			return;
		}
		globals.user.changePassword(oldPwRef.current.value,newPwRef.current.value).then(() => {
			p.done();
		},e => {
			// This requires some serious changes to work nicely... sigh.
			e.then((r:any) => {
				messageRef.current.innerText = TG.get(`error.${r.error}`);
			});
		});
	}
	return <form onSubmit={e => changePassword(e)}>
		<label>{oldPw}
			<input required type="password" ref={oldPwRef} placeholder={oldPw}/>
		</label>
		<label>{newPw}
			<input required type="password" ref={newPwRef} placeholder={newPw}/>
		</label>
		<label>{newPwConfirm}
			<input required type="password" ref={newPwConfirmRef} placeholder={newPwConfirm}/>
		</label>
		<span ref={messageRef} className="registerFormMessage errorText"/>
		<div className="dialogButtons">
			<input type="submit" value={TG.get("general.doIt")}/>
			<NevermindButton click={() => p.done()}/>
		</div>
		</form>
}
interface ICurrentUserProps{
	user:IUser;
	sessions:ISession[];
}
const mapInfoProps = (s:IState):ICurrentUserProps => {
	if (s.session.user == null)
		return {
			user:null,
			sessions:[]
		}
	if (s.session.sessions.length == 0) {
		globals.user.loadSessions();
	}
	return {
		user:s.session.user,
		sessions:s.session.sessions
	}
}
const CurrentUserInfoPresenter:React.FunctionComponent<ICurrentUserProps> = (p) => {
	let logOutFunc = () => {
		globals.user.logOut(true);
	}
	let deleteFunc = () => {
		globals.popup.showDialog(<DeleteUserConfirm done={() => globals.popup.hide()}/>);
	}
	let changePasswordFunc = () => {
		globals.popup.showDialog(<ChangePassword done={() => globals.popup.hide()}/>);
	}
	if (p.user == null)
		return <p>{TG.get("user.notLoggedIn")}</p>
	return <div>
		<UserInfo user={p.user}/>
		<h3>{TG.get("user.sessions")}</h3>
		<ul>
			{p.sessions.map(s => <li key={s.id}><em>{dateToIsoString(fromUnixTime(s.created))}</em> -- {s.name}</li>)}
		</ul>
		<input type="button" value={TG.get("user.changePassword")} onClick={() => changePasswordFunc()}/>
		<input type="button" value={TG.get("user.logOutAll")} onClick={() => logOutFunc()}/>
		<input type="button" value={TG.get("user.delete")} onClick={() => deleteFunc()}/>
	</div>
}
export const CurrentUserInfo = connect(mapInfoProps)(CurrentUserInfoPresenter);
type UserInfoParams = {id:string}
interface IUserInfoProps{
	userId?:number;
	user?:IUser;
	match?:match<UserInfoParams>;
	// Only used by the big UserInfo. Should probably cleanly split this.
	inventories?:IInventory[];
	items?:IItemListItem[];
}
const mapUserProps = (s:IState,p:IUserInfoProps):IUserInfoProps => {
	if (p.user != undefined)
		return {user:p.user,userId:p.user.id}
	let id = p.userId;
	if (id == undefined){
		id = Number(p.match.params.id);
	}
	let user = s.users.find(u => u.id == id);
	// This isn't the right way of doing this because it will result in getting a user many times.
	if (user == undefined)
		globals.user.getUser(id);
	return {user,userId:id}
}
const mapUserPropsBig = (s:IState, p:IUserInfoProps):IUserInfoProps => {
	let props = mapUserProps(s,p);
	props.inventories = s.inventories.filter(i => i.ownerId == props.userId);
	const items = s.items.filter(i => i.ownerId == props.userId);
	props.items = items.map((i) => {
		if (!s.session.user)
			return {id:i.id, itemId:i.id, actions:[]};
		return {id:i.id, itemId:i.id, actions:[],standardActions:true}
	});
	return props;
}
const UserInfoSmallPresenter:React.FunctionComponent<IUserInfoProps> = (p) => {
	let name = p.user == undefined ? TG.ges("user.unknown",p.userId.toString()) : p.user.friendlyName;
	return <NavLink to={`/user/${p.userId}`}>{name}</NavLink>
}
const UserInfoPresenter:React.FunctionComponent<IUserInfoProps> = (p) => {
	if (p.user == undefined)
		return <h1>{TG.ges("user.unknown",p.userId.toString())}</h1>
	return <div>
		<h1>{p.user.role == 1 && <span title={TG.get("user.admin")}>🔧</span>}{p.user.friendlyName}</h1>
		<h2>{p.user.username}</h2>
		<h3>{TG.get("inventory.their")}</h3>
		<div className="inlineBlock">
			<InventorySelector asLinks={true} ownerId={p.user.id}/>
		</div>
		<h3>{TG.get("item.their")}</h3>
		<ItemList detailsInDialog={false} items={p.items}/>
	</div>
}
export const UserInfoSmall = connect(mapUserProps)(UserInfoSmallPresenter);
export const UserInfo = connect(mapUserPropsBig)(UserInfoPresenter);
