import * as React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { IUser } from "../redux/types";
import { IState } from "../redux/store";
import { TG, globals } from "../globals";

interface IUserListProps{
	users:IUser[];
}
const mapUserListProps = (s:IState) => {
	return {users:s.users}
}
const UserListPresenter:React.FunctionComponent<IUserListProps> = (p) => {
	return <div>
		<ul>
			{p.users.map(u => <li key={u.id}><Link to={`/user/${u.id}`}><em>{u.username}</em> - {u.friendlyName}</Link></li>)}
		</ul>
	</div>
}
const UserList = connect(mapUserListProps)(UserListPresenter);

export class UserListView extends React.Component<{},{}>{
	componentDidMount(){
		globals.user.getAll();
	}
	render(){
		return <div>
			<h1>{TG.get("user.all")}</h1>
			<UserList/>
		</div>
	}
}