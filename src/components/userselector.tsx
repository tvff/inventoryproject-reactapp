import * as React from "react";
import { connect } from "react-redux";
import { IUser } from "../redux/types";
import { IState } from "../redux/store";
interface IUserSelectorPopupProps{
	isOpen:boolean;
}
const UserSelectorPopup:React.FunctionComponent<IUserSelectorPopupProps> = (p) => {
	if (!p.isOpen)
		return null;
	return <div className="popupMenu">
		<p>(not yet implemented, sorry!)</p>
	</div>
}
interface IUserSelectorProps{
	value?:IUser;
	users?:IUser[];
	current:number;
}
interface IUserSelectorState{
	isOpen:boolean;
}
class UserSelectorPresenter extends React.Component<IUserSelectorProps,IUserSelectorState>{
	state={
		isOpen:false
	}
	toggle(){
		this.setState({isOpen:!this.state.isOpen});
	}
	render(){
		return <button onClick={() => this.toggle()}>
		{this.props.value.friendlyName}
		<UserSelectorPopup isOpen={this.state.isOpen}/>
	</button>
	}
}
const mapUserSelectorProps = (s:IState, p:IUserSelectorProps) => {
	let value = s.users.find(u => u.id == p.current);
	if (!value)
		value={friendlyName:"?", username:"?",id:p.current,role:0};
	if (p.users){
		return Object.assign({},p,{value});
	}
	let users = s.users;
	return Object.assign({},p,{users,value});
}
export const UserSelector = connect(mapUserSelectorProps)(UserSelectorPresenter);