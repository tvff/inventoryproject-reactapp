import { UserService } from "./userservice";
import { InventoryService } from "./inventoryservice";
import { HTTPClient } from "./httpclient";
import { TextGet } from "./textget";
import { PopupManager } from "./components/popupmanager";
/**
 * Used for responsiveness!
 */
export enum AppState{
	Normal,
	Small
}

class AppGlobals{
	inventory:InventoryService;
	user:UserService;
	http:HTTPClient;
	textget:TextGet;
	popup:PopupManager;
	state:AppState;
	constructor(){
		this.http = new HTTPClient("/api");
		this.inventory = new InventoryService(this.http);
		// This getAllInventories thing is very very sketchy.
		this.user = new UserService(this.http, () => this.inventory.getAllInventories());
		this.textget = new TextGet();
		this.popup = new PopupManager();
		this.state = AppState.Normal;
		this.inventory.getAllItems();
	}
}

export const globals = new AppGlobals();
// Convenience export because TextGet is used a lot.
export const TG = globals.textget;