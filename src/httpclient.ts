/**
 * A basic HTTP client, so I don't have to write fetch boilerplate all the time!
 */
export class HTTPClient {
	private baseUri:string;
	defaultHeaders:any;
	cbErrorString:(e:string) => void;
	cbErrorJson:(e:any) => void;
	constructor(baseUri:string){
		this.baseUri = baseUri;
		this.cbErrorJson = (e:any) => {
			console.log("HTTPClient JSON error ", e);
		}
		this.cbErrorString = (e:string) => {
			console.log("HTTPClient error",e);
		}
		this.defaultHeaders = {};
	}
	private promiser(p:Promise<Response>):Promise<any>{
		return p.then(r => {
			if (!r.ok)
				return Promise.reject(r);
			return r.json();
		}).catch(e => {
			// Internal server error, probably a string dump..
			if (e.status == 500){
				e.text().then((t: string) => 
				{
					this.cbErrorString(t);
				});
			}
			// 502 = Bad Gateway, so the API server is probably dead.
			else if (e.status == 502){
				this.cbErrorString("502 bad gateway");
			}
			// Else it's probably a nice JSON object.. probably.
			else {
				try {
					e.json().then((j: any) => this.cbErrorJson(j));
				}
				// Damn, it was not JSON. This should never happen.
				catch (e) {
					this.cbErrorString(e);
				}
			}
			// Kick the rejection forward, this might cause an unhandled exception error in the console..
			// .. but it means the last then won't be executed. Which is what I want.
			return Promise.reject(e);
		});
	}
	get(uri:string):Promise<any>{
		return this.promiser(fetch(this.baseUri + uri,{headers:this.headers()}));
	}
	getBasic(uri:string):Promise<Response>{
		return fetch(this.baseUri + uri);
	}
	private postInternal(uri:string,body:any):Promise<any>{
		return fetch(this.baseUri + uri,{
			headers: this.headers({
				'Content-Type': 'application/json'}),
			method:'POST',
			body:JSON.stringify(body)
		});
	}
	post(uri:string,body:any):Promise<any>{
		return this.promiser(this.postInternal(uri,body));
	}
	patch(uri:string,body:any):Promise<any>{
		return this.promiser(fetch(this.baseUri + uri,{
			headers: this.headers({
				'Content-Type': 'application/json'}),
				method:'PATCH',
				body:JSON.stringify(body)
			})
		);
	}
	postBasic(uri:string,body:any):Promise<any>{
		return this.postInternal(uri,body);
	}
	put(uri:string,body:any):Promise<any>{
		let p = fetch(this.baseUri + uri,{
			headers: this.headers({'Content-Type': 'application/json'}),
			method:'PUT',
			body:JSON.stringify(body)
		})
		return this.promiser(p);
	}
	delete(uri:string):Promise<any>{
		let p = fetch(this.baseUri + uri,{
			method:'DELETE',
			headers:this.headers()
		})
		return this.promiser(p);
	}
	private headers(additional:any = null){
		if (additional == undefined)
			return this.defaultHeaders;
		return Object.assign(additional,this.defaultHeaders);
	}
}
