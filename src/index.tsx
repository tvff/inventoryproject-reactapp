import * as React from "react";
import * as ReactDOM from "react-dom";
import { MainComponent } from './components/main'
import { Provider } from "react-redux";
import { Store } from "./redux/store";
import { globals, AppState, TG } from "./globals";
const App = () => {
	return <React.StrictMode><Provider store={Store}><MainComponent/></Provider></React.StrictMode>
}
const setAppState = (big:boolean) => {
	if (big)
		globals.state = AppState.Normal;
	else
		globals.state = AppState.Small;
}
window.onload = () => {
	const mq = window.matchMedia("(min-width: 550px)");
	mq.addListener((query:MediaQueryListEvent) => {
		setAppState(query.matches);
	});
	setAppState(mq.matches);
	TG.loaded = () => {
		ReactDOM.render(<App/>,
			document.getElementById("main"));
	}
	TG.load('/lang/en.json');
}
