import { HTTPClient } from "./httpclient";
import { Store } from "./redux/store";
import * as Types from "./redux/types";
import * as ACreators from "./redux/actioncreators";
interface INewInventory{
	name:string;
	worldVisible:boolean;
	description:string;
}
interface INewTradeOffer{
	desiredItems:number[];
	offeredItems:number[];
	type:number;
	message:string;
	receiver:number;
	destination:number;
}
/**
 * The InventoryService is a service class that does all kinds of things in the app.
 * Rather than using callbacks, it updates the state in the Redux store.
 * However there should probably be some special handling for errors.
 */
export class InventoryService {
	private HC:HTTPClient;
	// The fetches currently being waited for. A hacky way of avoiding duplicate requests.
	private fetchesWaiting:string[];
	constructor(httpclient:HTTPClient){
		this.fetchesWaiting = [];
		this.HC = httpclient;
	}
	getItem(itemId:number){
		let uri = `/item/${itemId}`;
		// Don't do this if we are already waiting on this!
		if (this.fetchesWaiting.find(i => i == uri) != undefined)
			return;
		this.fetchesWaiting.push(uri);
		this.HC.getBasic(`/item/${itemId}`).then(r => {
			if (!r.ok) // Item not found, so remove all traces of it.
				Store.dispatch(ACreators.actionRemoveItem(itemId));
			else
				r.json().then(j => Store.dispatch(ACreators.actionEditItem(j)));
			this.fetchesWaiting.splice(this.fetchesWaiting.indexOf(uri),1);
		});
	}
	// This uses a callback instead. Look at how consistent I am.
	getOwnedItem(itemId:number,cb:(item:Types.IOwnedItem) => void){
		// I should make this pull from the store, but for now just hit the server.
		this.HC.get(`/item/owned/${itemId}`).then(r => {
			cb(r);
		});
	}
	getAllItems(){
		this.HC.get("/item").then(r => Store.dispatch(ACreators.actionReloadItems(r)));
	}
	createItem(name:string,cb?:(item:Types.IUniqueItem) => void){
		this.HC.post("/item",{name:name}).then(r => {
			Store.dispatch(ACreators.actionEditItem(r))
			// Ugly ugly hack.
			if (cb)
				cb(r);
		});
	}
	editItem(id:number,item:Types.INewUniqueItem){
		this.HC.put(`/item/${id}`,item).then(r => Store.dispatch(ACreators.actionEditItem(r)));
	}
	deleteItem(item:Types.IUniqueItem){
		this.HC.delete(`/item/${item.id}`).then(r => Store.dispatch(ACreators.actionRemoveItem(r.id)));
	}
	// Split all of this into different classes?
	getAllInventories(){
		this.HC.get("/inventory").then(r => {
			let inventories = <Types.IInventory[]>r;
			// Add empty arrays to the inventories. Just because.
			inventories.forEach(i => i.items = []);
			Store.dispatch(ACreators.actionReloadInventories(inventories));
		});
	}
	getInventoryItems(inv:Types.IInventory,cb?:() => void){
		this.HC.get(`/inventory/${inv.id}`).then(r => {
			Store.dispatch(ACreators.actionReloadInventoryItems(inv.id,r.items));
			if (cb)
				cb();
		});
	}
	createInventory(inv:INewInventory,cb:(inv:Types.IInventory) => void){
		this.HC.post('/inventory',inv).then(r => {
			Store.dispatch(ACreators.actionAddInventory(r));
			cb(r);
		});
	}
	editInventory(id:number,inv:INewInventory){
		this.HC.patch(`/inventory/${id}`,inv).then(r => Store.dispatch(ACreators.actionEditInventory(r)));
	}
	deleteInventory(inventory:Types.IInventory){
		this.HC.delete(`/inventory/${inventory.id}`).then(r => Store.dispatch(ACreators.actionRemoveInventory(r)));
	}
	putItemInInventory(item:Types.IUniqueItem,inventory:number){
		this.HC.put(`/inventory/${inventory}`,item).then(r => 
			Store.dispatch(ACreators.actionAddItemToInventory(inventory,r)));
	}
	removeItemFromInventory(ownedItemId:number,inventory:number){
		this.HC.delete(`/inventory/${inventory}/${ownedItemId}`).then(r => 
			Store.dispatch(ACreators.actionRemoveItemFromInventory(inventory,r.id)));
	}
	getPins(){
		if (Store.getState().session.user == undefined)
			return;
		this.HC.get("/user/pin").then(r => {
			Store.dispatch(ACreators.actionReloadPins(r));
		});
	}
	pinInventory(inventoryId:number){
		this.HC.post(`/user/pin/${inventoryId}`,{}).then(r => {
			Store.dispatch(ACreators.actionAddPin(inventoryId));
		});
	}
	unpinInventory(inventoryId:number){
		this.HC.delete(`/user/pin/${inventoryId}`).then(r => {
			Store.dispatch(ACreators.actionRemovePin(inventoryId));
		});
	}
	getTradeOffers(){
		this.HC.get("/trade").then(r => {
			Store.dispatch(ACreators.actionReloadTradeOffers(r));
		});
	}
	getTradeOffer(offerId:number){
		this.HC.get(`/trade/${offerId}`).then(r => {
			Store.dispatch(ACreators.actionAddTradeOffer(r));
		});
	}
	postTradeOffer(offer:INewTradeOffer,cb?:(id:number) => void){
		this.HC.post("/trade",offer).then(r => {
			Store.dispatch(ACreators.actionAddTradeOffer(r));
			if (cb)
				cb(r.id);
		});
	}
	deleteTradeOffer(offerId:number,cb?:() => void){
		this.HC.delete(`/trade/${offerId}`).then(r => {
			Store.dispatch(ACreators.actionRemoveTradeOffer(offerId));
			if (cb)
				cb();
		});
	}
	acceptTradeOffer(offerId:number,destination:number,cb?:() => void){
		this.HC.post(`/trade/${offerId}`,{response:'accept',destination}).then(r => {
			Store.dispatch(ACreators.actionRemoveTradeOffer(offerId));
			if (cb)
				cb();
		});
	}
	rejectTradeOffer(offerId:number,cb?:() => void){
		this.HC.post(`/trade/${offerId}`,{response:'decline'}).then(r => {
			Store.dispatch(ACreators.actionRemoveTradeOffer(offerId));
			if (cb)
				cb();
		});
	}
}
