import { EActionKeys } from "./actions";
import { IUniqueItem, IInventory, IOwnedItem,ISession, IUser, ITradeOffer } from "./types";
import * as AI from "./actions_items";
import * as II from "./actions_inventories";
export const actionReloadItems = (items:IUniqueItem[]):AI.AReloadItems => {
	return {
		items,
		type: EActionKeys.RELOAD_ITEMS
	};
}
export const actionEditItem = (item:IUniqueItem):AI.AEditItem => {
	return {
		item,
		type: EActionKeys.EDIT_ITEM
	}
}
export const actionRemoveItem = (itemId:number):AI.ARemoveItem => {
	return {
		itemId,
		type: EActionKeys.REMOVE_ITEM
	};
}
export const actionReloadInventories = (inventories:IInventory[]):II.AReloadInventories => {
	return {
		inventories,
		type: EActionKeys.RELOAD_INVENTORIES
	}
}
export const actionAddInventory = (inventory:IInventory):II.AAddInventory => {
	return {
		inventory,
		type: EActionKeys.ADD_INVENTORY
	}
}
export const actionEditInventory = (inventory:IInventory):II.AEditInventory => {
	return {
		inventory,
		type: EActionKeys.EDIT_INVENTORY
	}
}
export const actionRemoveInventory = (inventory:IInventory):II.ARemoveInventory => {
	return {
		id:inventory.id,
		type: EActionKeys.REMOVE_INVENTORY
	}
}
export const actionReloadInventoryItems = (inventoryId:number,items:IOwnedItem[]) => {
	return {
		inventoryId,
		items,
		type: EActionKeys.RELOAD_INVENTORY_ITEMS
	}
}
export const actionAddItemToInventory = (inventoryId:number,item:IOwnedItem) => {
	return {
		inventoryId,
		item,
		type: EActionKeys.ADD_ITEM_TO_INVENTORY
	}
}
export const actionRemoveItemFromInventory = (inventoryId:number,itemId:number) => {
	return {
		inventoryId,
		itemId,
		type: EActionKeys.REMOVE_ITEM_FROM_INVENTORY
	}
}
export const actionAddPin = (inventoryId:number) => {
	return {
		inventoryId,
		type: EActionKeys.ADD_PIN
	}
}
export const actionRemovePin = (inventoryId:number) => {
	return {
		inventoryId,
		type: EActionKeys.REMOVE_PIN
	}
}
export const actionReloadPins = (pins:number[]) => {
	return {
		pins,
		type: EActionKeys.RELOAD_PINS
	}
}
export const actionReloadTradeOffers = (offers:ITradeOffer[]) => {
	return {
		offers,
		type: EActionKeys.RELOAD_TRADE_OFFERS
	}
}
export const actionAddTradeOffer = (offer:ITradeOffer) => {
	return {
		offer,
		type: EActionKeys.ADD_TRADE_OFFER
	}
}
export const actionRemoveTradeOffer = (id:number) => {
	return {
		id,
		type: EActionKeys.REMOVE_TRADE_OFFER
	}
}
export const actionSetLanguage = (language:string) => {
	return {
		language,
		type: EActionKeys.SET_LANGUAGE
	}
}
export const actionLoggedIn = (session:ISession, user:IUser) => {
	return {
		session,
		user,
		type: EActionKeys.LOGGED_IN
	}
}
export const actionSessionsLoaded = (sessions:ISession[]) => {
	return {
		sessions,
		type: EActionKeys.SESSIONS_LOADED
	}
}
export const actionLoggedOut = () => {
	return {
		type: EActionKeys.LOGGED_OUT
	}
}
export const actionAddUser = (user:IUser) => {
	return {
		type: EActionKeys.ADD_USER,
		user
	}
}
export const actionReloadUsers = (users:IUser[]) => {
	return  {
		type: EActionKeys.RELOAD_USERS,
		users
	}
}
