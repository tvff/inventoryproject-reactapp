import { IUniqueItem } from "./types";
import * as A from "./actions_items";
import * as I from "./actions_inventories";
import * as O from "./actions_other";
export type ActionTypes =
	| A.AReloadItems
	| A.AEditItem
	| A.ARemoveItem
	| I.AAddInventory
	| I.AEditInventory
	| I.AReloadInventories
	| I.ARemoveInventory
	| I.AReloadInventoryItems
	| I.AAddItemToInventory
	| I.ARemoveItemFromInventory
	| I.AAddPin
	| I.ARemovePin
	| I.AReloadPins
	| I.AAddTradeOffer
	| I.ARemoveTradeOffer
	| I.AReloadTradeOffers
	| O.AAddUser
	| O.AReloadUsers
	| O.ASetLanguage
	| O.ALoggedIn
	| O.ALoggedOut
	| O.ASessionsLoaded
	| AUnknown;

export interface AUnknown {
	type: EActionKeys.UNKNOWN,
}
export enum EActionKeys {
	// UniqueItems
	EDIT_ITEM = "EDIT_ITEM",
	RELOAD_ITEMS = "RELOAD_ITEMS",
	REMOVE_ITEM = "REMOVE_ITEM",
	// Inventories
	RELOAD_INVENTORIES = "RELOAD_INVENTORIES",
	ADD_INVENTORY = "ADD_INVENTORY",
	EDIT_INVENTORY = "EDIT_INVENTORY",
	REMOVE_INVENTORY = "REMOVE_INVENTORY",
	RELOAD_INVENTORY_ITEMS = "RELOAD_INVENTORY_ITEMS",
	ADD_ITEM_TO_INVENTORY = "ADD_ITEM_TO_INVENTORY",
	REMOVE_ITEM_FROM_INVENTORY = "REMOVE_ITEM_FROM_INVENTORY",
	RELOAD_PINS = "RELOAD_PINS",
	ADD_PIN = "ADD_PIN",
	REMOVE_PIN = "REMOVE_PIN",
	// Trade offers (goes in user data)
	ADD_TRADE_OFFER = "ADD_TRADE_OFFER",
	REMOVE_TRADE_OFFER = "REMOVE_TRADE_OFFER",
	RELOAD_TRADE_OFFERS = "RELOAD_TRADE_OFFERS",
	// Users
	ADD_USER = "ADD_USER",
	RELOAD_USERS = "RELOAD_USERS",
	//
	LOGGED_IN = "LOGGED_IN",
	LOGGED_OUT = "LOGGED_OUT",
	SESSIONS_LOADED = "SESSIONS_LOADED",
	SET_LANGUAGE = "SET_LANGUAGE",
	UNKNOWN = "Something completely insane!"
}
