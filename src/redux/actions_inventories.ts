import { EActionKeys } from "./actions";
import { IInventory, IOwnedItem, ITradeOffer } from "./types";

export interface AAddInventory {
	type: EActionKeys.ADD_INVENTORY;
	inventory:IInventory;
}
export interface AEditInventory {
	type: EActionKeys.EDIT_INVENTORY;
	inventory:IInventory;
}
export interface ARemoveInventory {
	type: EActionKeys.REMOVE_INVENTORY;
	id:number
}
export interface AReloadInventories {
	type: EActionKeys.RELOAD_INVENTORIES;
	inventories:IInventory[];
}
export interface AReloadInventoryItems {
	type: EActionKeys.RELOAD_INVENTORY_ITEMS;
	items: IOwnedItem[];
	inventoryId: number;
}
export interface AAddItemToInventory {
	type: EActionKeys.ADD_ITEM_TO_INVENTORY;
	item:IOwnedItem;
	inventoryId:number;
}
export interface ARemoveItemFromInventory {
	type: EActionKeys.REMOVE_ITEM_FROM_INVENTORY;
	itemId:number;
	inventoryId:number;
}
export interface AAddPin {
	type: EActionKeys.ADD_PIN;
	inventoryId:number;
}
export interface ARemovePin {
	type: EActionKeys.REMOVE_PIN;
	inventoryId:number;
}
export interface AReloadPins {
	type: EActionKeys.RELOAD_PINS;
	pins:number[];
}
export interface AAddTradeOffer{
	type: EActionKeys.ADD_TRADE_OFFER;
	offer:ITradeOffer;
}
export interface ARemoveTradeOffer{
	type: EActionKeys.REMOVE_TRADE_OFFER;
	id:number;
}
export interface AReloadTradeOffers{
	type: EActionKeys.RELOAD_TRADE_OFFERS;
	offers:ITradeOffer[];
}