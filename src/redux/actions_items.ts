import { EActionKeys } from "./actions";
import { IUniqueItem } from "./types";

export interface AEditItem{
	type: EActionKeys.EDIT_ITEM;
	item: IUniqueItem;
}
export interface ARemoveItem {
	type: EActionKeys.REMOVE_ITEM;
	itemId: number;
}
export interface AReloadItems {
	type: EActionKeys.RELOAD_ITEMS;
	items: IUniqueItem[];
}
