import { EActionKeys } from "./actions";
import { ISession, IUser } from "./types";
export interface ASetLanguage {
	type:EActionKeys.SET_LANGUAGE,
	language:string;
}
export interface ALoggedIn {
	type:EActionKeys.LOGGED_IN,
	session:ISession,
	user:IUser
}
export interface ALoggedOut {
	type:EActionKeys.LOGGED_OUT,
}
export interface ASessionsLoaded {
	type:EActionKeys.SESSIONS_LOADED
	sessions:ISession[]
}
export interface AAddUser {
	type:EActionKeys.ADD_USER,
	user:IUser
}
export interface AReloadUsers {
	type:EActionKeys.RELOAD_USERS,
	users:IUser[]
}