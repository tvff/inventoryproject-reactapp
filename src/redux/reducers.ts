import { ActionTypes, EActionKeys } from './actions';
import { combineReducers } from 'redux';
import { IUniqueItem, IInventory, ISessionData, IUser, IUserData } from './types';
import { quickFindInArrayById } from '../array_util';
const itemReducer = (s:IUniqueItem[], a:ActionTypes) => {
	if (s == undefined)
		return [];
	let temp = [...s]
	switch (a.type) {
		case EActionKeys.EDIT_ITEM: {
			let item = quickFindInArrayById(temp,a.item.id);
			if (item.object == undefined){
				// It doesn't exist, so create it.
				if (item.index == temp.length)
					temp.push(a.item);
				else if (item.index == 0)
					temp.unshift(a.item);
				else{
					// My binary search implementation is a bit bad, so do this crazy stuff here.
					if (temp[item.index].id > a.item.id){
						if (temp[item.index-1].id < a.item.id){
							temp.splice(item.index,0,a.item);
							return temp;
						}
					}
					//console.log(`quickFind wants it at:\n ${temp[item.index-1].id}\n>${temp[item.index].id} -> ${a.item.id}\n ${temp[item.index+1].id}`);
					temp.splice(item.index+1,0,a.item);
				}
			}
			else {
				// It exists, so edit it.
				temp[item.index] = a.item;
			}
			return temp;
		}
		case EActionKeys.REMOVE_ITEM:
			let item = quickFindInArrayById(temp,a.itemId);
			if (item.object == undefined){
				console.log("Attempted to remove an item that doesn't exist. This should never happen!",a.itemId);
				return s;
			}
			temp.splice(item.index,1);
			return temp;
		case EActionKeys.RELOAD_ITEMS: {
			// This should actually come sorted from the server...
			return [...a.items].sort((a,b) => a.id - b.id);
		}
		default:
			return s;
	}
}
const inventoryReducer = (s:IInventory[], a:ActionTypes) => {
	if (s == undefined)
		return [];
	let temp = Array.from(s);
	switch (a.type) {
		case EActionKeys.ADD_INVENTORY:
			temp.push(a.inventory);
			return temp;
		case EActionKeys.EDIT_INVENTORY:
			let index = temp.findIndex(i => i.id == a.inventory.id);
			let tempInv = Object.assign({},temp[index]);
			tempInv.description = a.inventory.description;
			tempInv.name = a.inventory.name;
			tempInv.worldVisible = a.inventory.worldVisible;
			temp[index] = tempInv;
			return temp;
		case EActionKeys.REMOVE_INVENTORY:
			temp.splice(temp.findIndex(i => i.id == a.id),1);
			return temp;
		case EActionKeys.RELOAD_INVENTORIES:
			return a.inventories;
		// Evil duplicated code..
		case EActionKeys.RELOAD_INVENTORY_ITEMS: {
			let index = temp.findIndex(i => i.id == a.inventoryId);
			let newInventory = Object.assign({},temp[index],{items:a.items});
			temp[index] = newInventory;
			return temp;
		}
		case EActionKeys.ADD_ITEM_TO_INVENTORY: {
			let index = temp.findIndex(i => i.id == a.inventoryId);
			let tempItems = Array.from(temp[index].items);
			if (tempItems.find(it => it.id == a.item.id)) {
				console.log("Item already exists in inventory",a.item);
				return s;
			}
			tempItems.push(a.item);
			let newInventory = Object.assign({},temp[index],{items:tempItems});
			temp[index] = newInventory;
			return temp;
		}
		case EActionKeys.REMOVE_ITEM_FROM_INVENTORY: {
			let index = temp.findIndex(i => i.id == a.inventoryId);
			let tempItems = Array.from(temp[index].items);
			tempItems.splice(tempItems.findIndex(i => i.id == a.itemId),1);
			let newInventory = Object.assign({},temp[index],{items:tempItems});
			temp[index] = newInventory;
			return temp;
		}
		case EActionKeys.REMOVE_ITEM:
			return removeItemFromAllInventories(s,a.itemId);
		default:
			return s;
	}
}
const removeItemFromAllInventories = (s:IInventory[],itemId:number) => {
	let newInventories = Array.from(s);
	// This is a bit kludgy, but I'm not allowed to mutate the state.
	// I'm amazed it even worked when I was accidentally mutating the state.
	for (let i = 0; i < s.length;i++) {
		let inv = newInventories[i];
		let index = -1;
		let tempItems = Array.from(inv.items);
		while ((index = tempItems.findIndex(it => it.itemId == itemId)) != -1) {
			tempItems.splice(index,1);
		}
		newInventories[i] = Object.assign({},inv,{items:tempItems});
	}
	return newInventories;
}
const languageReducer = (s:string,a:ActionTypes) => {
	if (s == undefined)
		return "none";
	switch (a.type) {
		case EActionKeys.SET_LANGUAGE:
			return a.language;
		default: return s;
	}
}
const sessionReducer = (s:ISessionData,a:ActionTypes):ISessionData => {
	if (s == undefined)
		return {user:null,sessions:[],currentSession:null}
	switch (a.type) {
		case EActionKeys.LOGGED_IN:
			return {
				currentSession:a.session,
				user:a.user,
				sessions:[]
			}
		case EActionKeys.LOGGED_OUT:
			return {sessions:[],user:null,currentSession:null}
		case EActionKeys.SESSIONS_LOADED:
			return {
				sessions:a.sessions,
				currentSession:s.currentSession,
				user:s.user
			}
		default: return s;
	}

}
const userReducer = (s:IUser[],a:ActionTypes):IUser[] => {
	if (s == undefined)
		return [];
	switch (a.type){
		case EActionKeys.ADD_USER:
			return addUser(s,a.user);
		case EActionKeys.LOGGED_IN:
			return addUser(s,a.user);
		case EActionKeys.RELOAD_USERS:
			return a.users;
		default:return s;
	}
}
const addUser = (s:IUser[],user:IUser):IUser[] => {
	if (s.findIndex(u => u.id == user.id) != -1)
		return s;
	let temp = Array.from(s);
	temp.push(user);
	return temp;
}
const userDataReducer = (s:IUserData,a:ActionTypes):IUserData => {
	if (s == undefined)
		return {pins:[],tradeoffers:[]}
	switch (a.type){
		case EActionKeys.RELOAD_PINS:
			return Object.assign({},s,{pins:a.pins});
		case EActionKeys.ADD_PIN:{
			let pins = Array.from(s.pins);
			pins.push(a.inventoryId);
			return Object.assign({},s,{pins});
		}
		case EActionKeys.REMOVE_PIN:{
			let pins = Array.from(s.pins);
			let index = pins.indexOf(a.inventoryId);
			pins.splice(index,1);
			return Object.assign({},s,{pins});
		}
		case EActionKeys.RELOAD_TRADE_OFFERS:{
			return Object.assign({},s,{tradeoffers:a.offers});
		}
		case EActionKeys.ADD_TRADE_OFFER:{
			let tradeoffers = Array.from(s.tradeoffers)
			// This action is also used for updating offers.
			let index = tradeoffers.findIndex(i => i.id == a.offer.id);
			if (index != -1){
				tradeoffers[index] = a.offer;
			}
			else{
				tradeoffers.push(a.offer);
			}
			return Object.assign({},s,{tradeoffers});
		}
		case EActionKeys.REMOVE_TRADE_OFFER:{
			let tradeoffers = Array.from(s.tradeoffers)
			let index = tradeoffers.findIndex(t => t.id == a.id);
			if (index == -1){
				console.log("Tried to remove nonexistent trade offer",a.id);
				return s;
			}
			tradeoffers.splice(index,1);
			return Object.assign({},s,{tradeoffers});
		}
		case EActionKeys.LOGGED_OUT:
			return {pins:[],tradeoffers:[]}
		default:return s;
	}

}
export const allReducers = combineReducers({
	items:itemReducer,
	inventories:inventoryReducer,
	language:languageReducer,
	session:sessionReducer,
	users:userReducer,
	userdata:userDataReducer});
