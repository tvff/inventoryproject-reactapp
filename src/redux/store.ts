import { createStore } from "redux";
import { allReducers } from "./reducers";
import * as Types from "./types";
export interface IState {
	items: Types.IUniqueItem[];
	inventories : Types.IInventory[];
	language : string;
	session : Types.ISessionData;
	users : Types.IUser[];
	userdata : Types.IUserData;
}
export type AppState = ReturnType<typeof allReducers>;
const initStore = () => {
	let devtools:any = window[<any>"__REDUX_DEVTOOLS_EXTENSION__"];
	if (devtools != undefined) {
		console.log("Redux devtools enabled :)");
		return createStore(allReducers,devtools());
	}
	else
		return createStore(allReducers);
}
export const Store = initStore();
