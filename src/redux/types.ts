export interface IUniqueItem {
	id:number;
	name:string;
	createdAt:number;
	modifiedAt:number;
	description:string;
	ownerId:number;
}
export interface INewUniqueItem {
	name:string;
	description:string;
}
export interface IInventory {
	id:number;
	ownerId:number;
	name:string;
	description:string;
	worldVisible:boolean;
	items:IOwnedItem[];
}
export interface IOwnedItem {
	id:number;
	itemId:number;
	inventoryId:number;
}
export interface IUser {
	friendlyName:string;
	username:string;
	id:number;
	role:number;
}
export interface IUserData{
	pins:number[];
	tradeoffers:ITradeOffer[];
}
export interface ISessionData{
	user:IUser;
	sessions:ISession[];
	currentSession:ISession;
}
export interface ISession {
	id:number;
	token:string;
	created:number;
	expires:number;
	name:string;
}
export interface ITradeOffer{
	id:number;
	message:string;
	time:number;	
	type:number;
	desiredItems:IOwnedItem[];
	offeredItems:IOwnedItem[];
	sender:number;
	receiver:number;
}