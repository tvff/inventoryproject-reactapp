import { Store } from "./redux/store";
import { actionSetLanguage } from "./redux/actioncreators";
/**
 * This class is used for getting all UI text strings.
 * English is going to be the only language but this is good practice I guess.
 */
export class TextGet{
	strings:any;
	language:string;
	loaded:() => void;
	constructor(){
		this.strings={};
		this.language="NONE!";
		this.loaded = () => console.log("Language loaded. And the programmer forgot to set a loaded callback!");
	}
	load(uri:string){
		fetch(uri).then(r => r.json().then(r => {
			this.language = r.language;
			this.strings = r.strings;
			Store.dispatch(actionSetLanguage(r.language));
			this.loaded();
		}));
	}
	get(text:string):string{
		let result = text.split('.').reduce((a,b) => a[b], this.strings);
		if (result != undefined)
			return result;
		else
			return `!!! STRING NOT FOUND: ${text}`;
	}
	// GEt Substituted ;)
	// This is a rather poorly implemented version of this. Surely will be a source of chaos and fun.
	ges(text:string,...args: string[]):string{
		let str = this.get(text);
		if (args.length > 0) {
			let split = str.split('§') // Should probably use a regexp so a literal § can be inserted.
			if (split.length < args.length) {
				console.log(`not enough § to substitute!`,str,args);
				return str;
			}
			let combined = [];
			for (let i = 0; i < args.length;i++) {
				combined.push(split[i],args[i]);
			}
			if (split.length > args.length)
				combined.push(split[split.length-1]);
			return combined.join("");
		}
		return str;
	}
}