import { HTTPClient } from "./httpclient";
import { ISession, IUser } from "./redux/types";
import { Store,IState } from "./redux/store";
import { actionLoggedOut,actionLoggedIn, actionSessionsLoaded, actionAddUser, actionReloadUsers } from "./redux/actioncreators";
import { globals } from "./globals";
import { findInArrayById } from "./array_util";
interface ISessionData {
	sessions:ISession[];
	currentSession:number;
	user:IUser;
}
export interface ISessionAndUser {
	session:ISession;
	user:IUser;
}
/**
 * The User service class. Does all the user related things for the app.
 */
export class UserService{
	private http:HTTPClient;
	sessionsloaded:boolean;
	// The users being fetched.
	private fetching:number[];

	constructor(httpclient:HTTPClient, inited:() => void){
		this.http = httpclient;
		this.fetching = [];
		Store.subscribe(() => this.updateState());
		this.sessionsloaded = false;
		let sess = window.localStorage.getItem('sessiontoken');
		if (sess != null)
			this.testSessionToken(sess, inited);
		else
			inited();
	}
	private updateState() {
		let state = <IState>Store.getState();
		if (state.session.user != null)
			this.setToken(state.session.currentSession.token);
		else
			this.setToken(undefined);
	}
	private setToken(token:string){
		if (token != undefined){
			this.http.defaultHeaders = {'X-Session':token};
			window.localStorage.setItem('sessiontoken',token);
		}
		else {
			window.localStorage.removeItem('sessiontoken');
			this.http.defaultHeaders = {};
			this.sessionsloaded = false;
		}
	}
	private defaultPromise(p:Promise<Response>):Promise<any>{
		return p.then(r => {
			if (r.status == 502) {
				return Promise.reject(Promise.resolve({error:"502",message:"Bad gateway!"}));
			}
			else if (!r.ok)
				return Promise.reject(r.json());
			return r.json();
		});
	}
	private testSessionToken(token:string, inited:() => void){
		this.setToken(token);
		this.getSessions().then(p => {
			let realsession = findInArrayById(p.sessions,p.currentSession);
			realsession.token = token;
			console.log("Using saved session",realsession);
			this.useSessionUser({session:realsession,user:p.user});
			Store.dispatch(actionSessionsLoaded(p.sessions));
			this.sessionsloaded = true;
		},e => {
			console.log("Saved session was invalid, throwing it out...");
			this.setToken(undefined);
			inited();
		});
	}
	useSessionUser(session:ISessionAndUser){
		Store.dispatch(actionLoggedIn(session.session,session.user));
		// Hastily reload inventories now.
		globals.inventory.getAllInventories();
		// And pins.
		globals.inventory.getPins();
	}
	register(username:string,friendlyname:string,password:string):Promise<IUser>{
		let p = this.http.postBasic('/user/register',{username,password,friendlyname});
		return this.defaultPromise(p);
	}
	authenticate(username:string,password:string):Promise<ISessionAndUser>{
		let p = this.http.postBasic('/user/authenticate',{username,password});
		return this.defaultPromise(p);
	}
	getSessions():Promise<ISessionData>{
		let p = this.http.get('/user/session');
		return p;
	}
	// Hackyhack to update sessions list on login.
	// Will have to do this properly sooner or later.
	loadSessions(){
		if (this.sessionsloaded)
			return;
		this.sessionsloaded = true;
		this.getSessions().then(s => {
			Store.dispatch(actionSessionsLoaded(s.sessions));
		});
	}
	logOut(allSessions:boolean = false){
		this.http.post('/user/logout',{allSessions}).then(p => {
			Store.dispatch(actionLoggedOut());
			// Refresh inventories here as well.
			globals.inventory.getAllInventories();
		});
	}
	getUser(id:number){
		if (this.fetching.some(u => u == id))
			return;
		this.fetching.push(id);
		this.http.get(`/user/${id}`).then(p => {
			Store.dispatch(actionAddUser(p));
			this.fetching.splice(this.fetching.findIndex(u => u == id),1);
		});
	}
	getAll(){
		this.http.get('/user').then(p => {
			Store.dispatch(actionReloadUsers(p));
		});
	}
	changePassword(oldPassword:string,password:string):Promise<any>{
		let state = Store.getState();
		if (!state.session.currentSession)
			throw "attempted to change password while not logged in... how did this even happen?";
		let promise = this.http.postBasic('/user/changepassword',{oldPassword,password});
		return this.defaultPromise(promise);
	}
	userDeleted(){
		Store.dispatch(actionLoggedOut());
	}
	deleteUser(password:string):Promise<any>{
		let state = Store.getState();
		if (state.session.currentSession == undefined)
			return;
		let p = this.http.postBasic('/user/delete',{username:state.session.user.username,password});
		return this.defaultPromise(p);
	}
}
