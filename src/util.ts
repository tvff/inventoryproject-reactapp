export const stringSortCompare = (a:string,b:string):number => {
	return a.toLocaleLowerCase().localeCompare(b.toLocaleLowerCase());
}
export const fromUnixTime = (unix:number):Date => {
	return new Date(unix*1000);
}
export const sliceString = (string:string,maxlength:number):string => {
	if (string.length > maxlength)
		return string.slice(0,maxlength) + '...';
	return string;
}
const padNumber = (numb:number):string => {
	if (numb > 9)
		return numb.toString();
	else
		return "0" + numb;
}
export const dateToIsoString = (date:Date) => {
	const month = padNumber(date.getMonth()+1);
	const day = padNumber(date.getDate());
	const hours = padNumber(date.getHours());
	const minutes = padNumber(date.getMinutes());
	const seconds = padNumber(date.getSeconds());
	return `${date.getFullYear()}-${month}-${day} ${hours}:${minutes}:${seconds}`;
}

/**
 * The most basic URL query string parser. Not to be used for anything serious.
 * @param query The query string, e.g. the bit that starts with a question mark.
 */
export const parseQueryString = (query:string):any => {
	// Sanity check
	if (query[0] != "?")
		throw "Query string didn't start with a question mark!"
	
	const pure = query.substring(1);
	const params = pure.split("&");
	let paramobj = Object.create(null);
	params.forEach(e => {
		const splittedparam = e.split("=");
		if (splittedparam.length == 2)
			paramobj[splittedparam[0]] = splittedparam[1];
	});
	return paramobj;
}
