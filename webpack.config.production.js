const path = require('path');
module.exports = {
	mode: "production",
	resolve: {
		extensions: [".ts", ".tsx",".js"]
	},
	output: {
		path: path.resolve(__dirname, "wwwroot/app"),
		filename: "main.js"
	},

	module: {
		rules: [
			{
				test: /\.ts(x?)$/,
				exclude: /node_modules/,
				use: [
					{
						loader: "awesome-typescript-loader"
					}
				]
			}
		]
	}
};
